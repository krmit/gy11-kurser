# Gemmensamt innehåll i IT-kurserna

![Tillsammans](../image/common.png)

Detta är sådant som kan komma att användas i alla kurserna. Alla examinationer här är test för att ni ska se hur det fungerar.

## Moment

- [Tools](./tools/index.html)

_Tillbaka till [Kurslista](../index.html)_

---

**Copyright © 2018-2020, Magnus Kronnäs**

All rights reserved.
