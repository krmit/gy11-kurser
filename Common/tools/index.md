# Innehåll

## Om det som är gemmensamt för våra kurser

![Verktyg](../../image/tools.png)

Här tar vi upp ett par verktyg och metoder som är gemmesamt för våra kurser.

## Översikt

- [Inlämning](./submission.sv.html).

## Info

- [Regler på inriktnignen.](https://drive.google.com/open?id=1khJfdN95vAYBe7MhEINjRbYr-A4YY2FTlP6Lirh7sV4)
- Några exempel på [mjukvaruutvecklare](https://docs.google.com/document/d/11CHx0coScSETXedwbadkvNk36hq5qJk2YQtb-nmNG5c/edit?usp=sharing) och vad de tycker.
- Läsårsplanering
  - Uppgifterna och lektionsplaneringarna på [Schoolsoft](https://sms8.schoolsoft.se/hassleholm/)
  - [Kursplaneringarna](http://htsit.se/k/)

## Kapitel

- [Introduktion till läromaterialet](http://htsit.se/a/topics/Introduction-to-this/)
- [Git](http://www.htsit.se/a/topics/programming/Git/index.html)
- [Gitlab](http://www.htsit.se/a/topics/programming/Git/05-Hosting-Service/index.html)
- [Programvarutveckling](http://htsit.se/a/topics/programming/Software-development/)

## Guider

- Guide till [hur man använder Git](https://docs.google.com/document/d/1OmJZtx9t_Sqcv3st_9IJZNqsGDTpuOgMyXv-NPonvIk/edit?usp=sharing)
- Guide till Valhall servern - Kommer senare i höst

_Tillbaka till [översikt](../index.html)_

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
