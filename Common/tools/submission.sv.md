# Inlämning om att hantera utvecklingsverktyg

## Grunderna i Git och Gitlab

![Verktyg](../../image/tools.png)

**Deadline:** _2019-09-05 22:00_

# Beskrivning

Vi ska öva oss på grunderna i hur man använder git och gitlab genom några praktiska övningar.

## Inlämning

- Du ska skicka svaret som en e-post till “magnus.kronnas@hassleholm.se”.
- Ämnet på brevet ska vara “gemensamt-0-git”.
- Du ska också skriva ditt namn i brevet.
- Är ämnet fel eller brevet skickas in för sent, dvs. inte dagen innan redovisning av uppgiften, kommer uppgiften inte att bedömas.
- På redovisningsdagen kommer du få svara på frågor om din inlämning.
- Obs! Det är viktigt att du lämnar in i tid.

Om du inte lämnar in i tid kommer det på SchoolSoft skickas ett meddelande med instruktioner för sen inlämning. Observera att det kommer antas att du fått F på detta moment fram till inlämning har gjorts och du därför också är F varnad.

## Bedömning

Din bedömning kommer bli den lägsta nivån som du får på de olika delarna nedan. Om du klarat en del på en högre nivå än de andra delen kommer ditt betyg bli det lägsta mellanbetyget som är högre än ditt lägsta resultat. Med andra ord om du får E och A, så blir ditt resultat D.

Observera att en bedömning på denna uppgift som visar att ditt resultat är F är också en F-varning. Du kommer då behöva lämna in uppgiften igen för att kunna bli godkänd på kursen. Du får instruktioner för detta via SchoolSoft.

Om du är missnöjd med betyg så kan du också lämna in uppgiften igen. Observera dock att du först ska skicka ett mail till mig så att du kan får instruktioner för hur du ska komplettera. För visa betyg kan också uppgiften förändras när du vill komplettera se kompletterings uppgifterna i slutet av uppgiften.

All diskussion om bedömningar av uppgiften via om e-post, läraren ger inte muntliga svar på detta förutom på resurstiden på fredag mellan 14.30 och 15.30. Sådan brev måste skickas in minst en vecka innan betygen sätts.

# Del I - Om att skapa ett liten portfolio i git

_Betyget på denna del blir det betyg som du klarat alla steg och villkor för. Om du klara väsentligt med steg och villkor på en högre nivå blir det ett mellanbetyg._

Du ska använda git och gitlab för att skapa en liten portfolio. Denna inlämning är tänk som ett test så du får lära dig hur inlämningar fungerar, men gör ändå ditt bästa för det visar din ambitionsnivå.

## Innehåll

Allt ifrån [beskrivnignen](http://htsit.se/k/Common/tools/index.html) av momentet som handlar om git och gitlab.

## Förberedelser

  * Se till att ha ett konto på gitlab.
  * Du bör att ha en linux terminal med tillgång till internet igång.
    * Men det går att lösa med bara gitlab WebIDE.
  * Du bör se till att du kan använda git via ssh nycklar.
  
## Redovisning

Svaret på denna uppgift är länken till det arkiv som du skapar nedan.

## Att göra

   * All markdown kod fungerar korrekt. (E)
   * All markdown kod är väl strukturerad. (C)
   * Du använder dig av flera olika funktionalitet som markdown har förutom rubriker.(A) 

1. Skapa ett projekt på gitlab enligt följande:
   1. Skapa ett projekt med namnet ditt alias + ”-framtid”. Till exempel “krm-framtid”.
   2. Projektet ska vara privat.
   3. Git ska vara konfigurerat med ditt användarnamn och e-postadress. Följ gitlab rekommendation. (C)
   4. Du ska ha en tom README fil.
   5. Första commiten ska ha kommentaren "Initial commit".
   6. Lägg till “krmit” som “Guest” till projektet.
   7. Klona ditt projekt till din dator.
    
2. (E) I detta projekt kopiera följande text till README. Du kan själv ändra det efter behag så länge du bevara syftet med projektet. Kommita med lämplig kommentar och pusha till gitlab.

```markdown
# Mina framtidsplaner!

I detta projekt kommer jag att samla anteckningar om vad jag ska göra i **framtiden**.

Så som:

  * CV
  * Personliga brev.
  * Utbildningsförslag.
  * Studieteknik
  * Förberedelser inför HP.
  * Intressanta länkar.
  * Foton och annan marknadsföringsmaterial av dig.
  * Portfolio eller länkar till en sådant som ska vara med i den.
```

*OBS! I övningarna nedan behöver du inte skriva mer än en mening. Det behöver inte vara seriöst eller personligt. Glöm inte bort commit kommentaren.*

3. Skapa en fil “Planer.md” i ditt projekt. I filen skriv:
   1. Skriv en rubrik i denna fil om hur du tänker din framtid. Commita  och pusha. (E)
   2. Skriv en kommentar om vilken utbildning du funderar på. Commita  och pusha. (C)
   3. Skriv en kommentar om hur du ska göra med högskoleprovet. Commita  och pusha. (C)
   4. Skriv en kommentar om hur du ska göra med betygen. Commita  och pusha. (C)
   5. Skriv en avslutning. Commita  och pusha. (C)
  
4. Skapa ärenden för detta projekt:
   1. Gör ett ärende som säger att du ska skapa en portfolio. (C)
   2. Gör ärenden som representerar allt som ska finnas i arkivet enligt din README. (A)
   3. Skapa en tag “osäker” och lägg till den minst ett av ärendena(A).
   4. Skapa ett ärende som innehåller en referens till ett annat ärende(A).

# Kompletering

Eftersom detta är till störsata delen en test inlämningsuppgift, så finns det inga kompletteringar på denna uppgift.

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
