# Innehåll
## Att utöfra arbetsuppgifter
![Att göra](../../image/todo.png)

Att utföra enkla arbetsuppgifter inom IT 

## Översikt

  * [Inlämning](http://htsit.se/e/data-5-arbetsuppift), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * Dokumentation
  * ESD-säkerhet
  * Skrivare
  * Installation av drivrutiner och firmware

## Guide

  * Git
  * Gitlab

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
