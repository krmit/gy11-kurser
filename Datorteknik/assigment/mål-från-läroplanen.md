# Mål från läroplanen
## 

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Installation av kringutrustning och uppgradering av drivrutiner.
  * Konfigurering av grafikkort.

## Kunskapskrav

### E                   


### C                   


### A                   


 
---
__Copyright © 2018-2019, Magnus Kronnäs__

All rights reserved.

