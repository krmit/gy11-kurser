# Innehåll
## Olika datorkomponeter och hur vi använder dem
![Moderkort](../../image/motherboard.png)

Olika datorkomponeter och hur vi använder dem.

## Översikt

  * [Inlämning](http://htsit.se/e/data-4-komponeter), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * Grundläggande datorarkitekturer
  * Datorns olika delar

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
