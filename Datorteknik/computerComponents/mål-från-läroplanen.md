# Mål från läroplanen
## Om datotns uppbyggnad

## Kommentarer om läroplannen 

_Tillbaka till [introduktion](./index.html)_

## Centralt innehåll

  * Hård- och ...installation, uppbyggnad, konfigurering och uppgradering av datorer och datorsystem.
  * Olika processortyper och deras användningsområden.
  * Prestanda för olika datalagringmedia. Lagring av data på optiska medier.
  * Interna och externa bussar, deras användningsområden och prestanda.
  * Installation av kringutrustning …
  * Installation och underhåll av lokal skrivare.
  * Konfigurering av grafikkort.
  * Kontroll och optimering av datorers och datorsystems prestanda och funktion.
  * Felsökning i datorer ....


## Kunskapskrav

### E                   

 * Eleven planerar och utför i samråd med handledare och med visst handlag hård- … installation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i datorer och … . 
 * Resultatet är tillfredsställande i fråga om funktion, säkerhet och kvalitet.
 * Eleven hanterar med visst handlag utrustning och verktyg samt utför arbetet på ett säkert sätt.
 * I arbetet använder eleven med viss säkerhet instruktioner, manualer, topologier och andra dokument på både svenska och engelska samt gör en enkel dokumentation av sitt arbete.
 * När eleven samråder med handledare bedömer hon eller han med viss säkerhet den egna förmågan och situationens krav.

### C                   

 * Eleven planerar och utför efter samråd med handledare och med visst handlag hård- … installation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i datorer och … . 
 * Eleven hanterar med gott handlag utrustning och verktyg samt utför arbetet på ett säkert sätt.
 * …  samt gör en noggrann dokumentation av sitt arbete.

### A                   

 * Eleven planerar och utför  … med mycket gott handlag avancerad hård- … installation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i datorer och … . 
 * Resultatet är gott i fråga om funktion, säkerhet och kvalitet.
 * I arbetet använder eleven med säkerhet instruktioner, manualer, topologier och andra dokument på både svenska och engelska samt gör en noggrann och utförlig  dokumentation av sitt arbete.
 * När eleven samråder med handledare bedömer hon eller han med säkerhet den egna förmågan och situationens krav.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

