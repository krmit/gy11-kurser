# Beskriv och analysera din drömdator

Du ska göra en beskrivning på din dröm dator eller beskriva en dator som löser ett intressant tillämpar uppgift. Du kan vara helt fri när du väljer projekt, men tänk på bedömningen nedan om du vill ha högt betyg. Om du vill att dina dator ska användas när vi designar nästa dator till IT-inriktningen måste du göra inhandlingen ifrån [ATEA](https://www.atea.se/eshop/) och utgå från priser utan moms, men det är inget krav och du kan använda den leverantör du är van med eller fråga om bra alternativ.

## Bedömning

Denan uppgift har bara en del. Besvara de fyra frågorna nedan utförligt och följ instruktionerna för att få ett bra betyg. Fråga 3 och 4 behöver du inte besvara om du inte vill ha högre betyg än D.

Observera att en bedömning som visar att ditt resultat är F är också en F-varning. du kommer då behöva göra komplettering för  E nivå.

# Uppgift

Välj ett intrssant projekt för datorbyggnad. Det kan vara det du vill, men läs bedömningarna nedan så att din uppgift bassar utifrån det som bedöms.

## Förslag:

  * Hur billig kan jag göra en bra speldator?
  * Hu bra dator kan jag bygga för 10 000 kr? Beloppet kan också vara 5000, 1500 eller vad du tycker är intressant.
  * Hur bra prestanda kan jag få in i en dator med kommersiellt tillgängliga delar?
  * Dator för digital bildbehandling
  * Dator lämplig för börshandel.
  * En bra serverdator med eller utan server anpassade delar.
  * Dator för kryptomining? Med eller utan raket.
  
## 1. Vad vill du göra?
  * Ge en kort men klar beskrivning av din vision för denna dator. Hur kommer den att vara bra att använda? (E)
  * Vilka vägval kommer du att göra när du väljer komponenter? (C)
  * Din vision ska vara baserat utifrån en förståelse för hur en dator fungerar, men också eftersträva något innovativt. (A)

## 2. Gör en bra lista
  * Gör en lista över alla komponenter du vill ha med i din dator. (E)
  * Du ska också redovisa den totala kostnaden för datorn. (E)
  * Det ska tydligt framgå vad varje komponent är för något, komponentens namn, var den går att köpa och hur mycket dessa kostar. (C)
  * Din lista ska vara snygg, överskådlig och praktiskt användbar. (A)

## 3. Beskriv några komponent som du valt.
  * Du ska gå igenom alla komponenter beskriv dem. (C)
  * Ge en utförlig beskrivning av ett urval av de viktigaste komponenterna utifrån ett datateknisk perspektiv och förklara hur de passar in i din helhetsidé. (A)

## 4. Gör en bra argumentation
  * Du gör en övertygande argumentation utifrån din helhatsidé om varför du valt de ingående komponenterna. Denna argumentation kan du även ha gjort under andra punkter men sammanfattar den här. (A)


## Kompletering

Om du inte bli klar i tid kommer du få följande kompletering:

### För E:  

  * Gör allt på E nivå på frågoran 1 och 2, men du måpste ocks ge ett svar på fråga 3.
  * __Slutprojekt:__ 

### För C:

  * Besvara alla frågor utifrån den högsta bedömningen ovan.

### För A:

  * Skriv till läraren och be om en extra uppgift.

---
__Copyright © 2018-2019, Magnus Kronnäs__

All rights reserved.
