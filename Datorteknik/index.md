# Dator- och nätverksteknik

![Första PC:n](../image/first_altair.png)

Darorteknik är om vad en datort är och hur vi använder den. En kurs där du i huvudsak får lära dig att hur du monterar installerar och felsöker datorer. Dessutom går vi igenom hur ett nätverk fungerar och får datorn att fungera i detta.

Observera att sista mometet "Göra arbetsuppgift" gås igenom i början av kursen, men måste inte lämnas in förän i maj.

## Moment

- [Instsallation av OS](./installation/index.html)
- [Använda Linux](./linux/index.html)
- [OSI](./network/index.html)
- [Datorkomponeter](./computerComponents/index.html)
- [Göra arbetsuppgifter](./assigment/index.html)

[**Kursplanen**](../plans/dator--och-kommunikationsteknik.pdf)

_Tillbaka till [Kurslista](../kurslista.html)_

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
