# Innehåll

## Om gunderna i olika operativsystem och installation av dem

![Installation](../../image/installation.png)

Vi ska lära oss om olika OS framförallt linux

## Översikt

- [Inlämning](./submission.sv.html).
- [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

- [Intro till Linux](http://htsit.se/a/topics/computer/os/linux/Introduction-to-linux/index.html)
- [Installation av Linux](http://htsit.se/a/topics/computer/os/linux/Installation-of-linux/index.html)
- [Linux som OS](http://www.htsit.se/a/topics/computer/os/linux/Introduction-to-linux/index.html)
- [Något om BSD](http://www.htsit.se/a/topics/computer/os/bsd/index.html)
- [Något om Windows](http://www.htsit.se/a/topics/computer/os/windows/index.html)

## Guider

- [Installation av Mint som ett ensamt OS på en dator](http://htsit.se/a/topics/computer/os/linux/Installation-of-linux/Tutorials/installation-of-Mint-as-a-single-OS.html)
- [Installation av Mint som en av många OS](http://htsit.se/a/topics/computer/os/linux/Installation-of-linux/Tutorials/installation-of-Mint-as-one-of-many-OS.html)

_Tillbaka till [översikt](../index.html)_

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
