# Mål från läroplanen
## 

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Vanliga operativsystem och deras egenskaper.
  * Säkerhetskopiering och virusskydd.

  * Datorns start- och bootsekvenser samt inställning och uppgradering av dess BIOS (Basic Input/Output System) eller firmware.
  * Felsökning i ... datorsystem.
  * Hård- mjukvaruinstallation, uppbyggnad, konfigurering och uppgradering av datorer och datorsystem.


## Kunskapskrav

### E                   

 * Eleven planerar och utför i samråd med handledare och med visst handlag ... mjukvaruinstallation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i ... datorsystem. 
 * Resultatet är tillfredsställande i fråga om funktion, säkerhet och kvalitet.
 * Dessutom installerar eleven i samråd med handledare datorer i lokala nätverk.
 * I arbetet använder eleven med viss säkerhet instruktioner, manualer, topologier och andra dokument på både svenska och engelska...

### C                   

 * Eleven planerar och utför efter samråd med handledare och med gott handlag ... mjukvaruinstallation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i ... datorsystem.
 * Dessutom installerar eleven efter samråd med handledare datorer i lokala nätverk. 

### A                   

 * Eleven planerar och utför  … med mycket gott handlag avancerad ... mjukvaruinstallation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i ... datorsystem. 
 * Resultatet är gott i fråga om funktion, säkerhet och kvalitet.
 * I arbetet använder eleven med säkerhet instruktioner, manualer, topologier och andra dokument på både svenska och engelska...
 
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

