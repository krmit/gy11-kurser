# Inlämningsuppgift om att installera OS

## Att installera olika versioner av linux

![Installation](../../image/installation.png)

**✋ Uppgiften inte klar ännu**

# Innehåll

Du ska visa att du behärskar grundläggande kunskaper om hur vi hanterar ett linux OS. Du kommer få en mapp med filer. Dessa ska modifieras utifrån instruktionerna och du ska redovisa vilka kommandon du använder.

## Bedömning

### Del I: Installation av Linux på din desktop dator

#### A

Din dator och ditt arbeta med att installera OS ska följa kraven nedan.

- Installerat ett “guest” os på datorn som följer de specifikationer som gest nedan.
  - Den första partitionen ska innehålla Linux Mint, som ska vara vårt guest OS. Detta är specifikationerna för systemet:
  - Språket ska vara Engelska med Svenska keyboard layout.
  - Datorns namn ska vara en sammansättning av “guestOS-” och ditt alias.
  - Med en användare “htsit”. Observera att den användare blir en så kalla “sudo” användare och får root.
  - Denna användare ska ha lösenordet: “vvgphtsitn”
- Installerad ett annat os på en annan partition på datorn.
- Installerar programvara på Linux med hjälp av apt-get
- Guest OS på datorn måste gå att boota upp.

#### C

- Installationer av minst två OS sker självständigt och utan hjälp av läraren.
- Installerar programvara på Linux från externa arkiv och kunna installera nedlade packet.
- Alla OS på datorn måste gå att boota från en boot meny.

#### A

Dessa installationer kan med fördel ske med virtualisering.

- Installerar mjukvara genom att kompilera källkod som följer den traditionella “./config; make; make install” proceduren.
- Ett av de installerade OS tillhör har ett mer krävande installations förfarande, såsom Arch, Gentoo, openBSD med flera.
- Minst tre olika OS har installerats. Vara av minst från två olika OS familjer såsom linux, BSD, windows eller något special OS.
- Alla installerade OS fungerar bra utan problem eller om problem finns ges en utförlig redovisning om vad problemet är och hur det har försökts rättas till.

### Del II: Gör alla uppgifter om Linux kommandon

Ladda ned filen med upggifter som du ska lösa på en Linux system och packa upp det, se länken nedan.
Observera att du måste ha rätt fil se listan nedan. När du redovisar ska du redovisa alla koamndon som du har använt under frågan.

1. Öppna filen “Del-I-X.txt”, Där bokstaven “X” står för vilken X-kategori du tillhör, du får veta vilken kategori du tillhör i början av provet.

#### E

\*. Gör minst hälften av uppgifter i “Del-I-X.txt”. Observera att du under varje uppgift ska skriva vilket kommando som du använder. Se också nedan för flera krav på hur du ska lösa uppgifterna för högre betyg.

#### C

- Du ska ha löst i stort sett alla uppgifter korrekt, men undantag för någon.

#### A

- Du ska använda som mest ett kommando för att lösa en uppgift.

## Kompletering

### E

- Gör allt på E nivå på del I och del II enligt variant som du fick i resultatet för detta test.
- Du ska också muntligen svara på frågor om de vanligaste unix komandona och vad de gör. Med de vanligste kommanodna menar vi här: ls, cd, mkdir, pwd, cp, rm och echo.

### C

- Gör del I på C nivå, men du ska installera ett extra OS och muntligt förklara hur ditt system fungerar.
- Begär att få en ny variant tilldelat för del II. OBS! Du måste fråga om vilken variant du ska ha annars kommer inte resultatet att räknas. Det blir inte samma variant som du hade innan deadline.
- Gör C nivå för del II och den varianten som du fått dig tilldelat.

### A

- Gör som för C med se till att du får A på del I och del II
- Du ska också installera Arch som linux OS, har du redan gjort det få du instellera Gentoo. Redovisa detta muntligt.
- När du gjort punkterna ovan ska du ska du begäran en ny variant för del III. Denna kommer varea en helt ny variant

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
