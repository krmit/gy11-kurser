# Kommentarer om Datorteknik

Ämnesplanen: [Dator- och nätverksteknik](https://www.skolverket.se/laroplaner-amnen-och-kurser/gymnasieutbildning/gymnasieskola/dao/subject.htm?subjectCode=dao&tos=gy)

_Tillbaka till [översikt](./översikt.html)_

## Kommentarer

Vi närmare genomläsning fick jag en annan bild av kursen än jag först tänkte mig, så här en förklaring. Kursen beskriver i huvudsak hur man hanterar, konfigurerar och installerar en personlig dator. Nätverksdelen består av att koppla in dator mot en switch. Övriga nätverks bitar såsom routing, switching och vpn är teoretiska och täcks bra med en genomgång av OSI. Observera också att för nätverk delen finns det ett extra krav på förståelse för betyg A. Därför kommer det vara mycket viktigt att skriva bra på OSI provet för att få A i slutbetyg eftersom kursen i övrigt är enkelt. När det gäller OS så ska installation göras och verktyg läras som kan användas för felsökning av nätverk och dator. Detta öppnar upp för undervisning av linux kommandon. Om man ser till examensmål kan det också motiveras att även kommandon för användning av linux OS lärs ut vilket ger en bättre helhet på denna del. Redovisningen av kunskap ska till större del ske i form av praktisk arbete men med stora krav på dokumentation. Vidare tolkar jag kursen som relativt enkelt. Jämfört med Datorteknik 1a som kursen ersätter har många nyttiga delar försvunnit och inget har egentligen ersatt dem utom möjligen en större betoning av praktiska moment. Noterbart är att även om kunskapskraven om dokumentation endast nämns explicit i sista uppgiften så är alla prov och inlämningar en del av detta kunskapskrav.

**OBS:** Kursplanen innehåller ett uppenbart fel. Det står att undervisning ska ske om BIOS. Men BIOS är ersatt av UEFI i (nästa?) alla moderna system. Däremot i mycket litteratur, inklusive anvädar manualler, kallas UEFI fortfarande för BIOS.


---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.
