# Innehåll
## Om anvädningen av Linux och grundläggande OS koncept.
![Linux](../../image/linux.png)

Vi ska lära oss om OS framförallt linux 

## Översikt
  * [Prov](http://htsit.se/e/data-2-linux)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * [Introduktion till Terminal](http://htsit.se/a/topics/computer/terminal/Introduction-to-terminal/index.html)
  * [Grundläggande kommandon i en terminal](http://htsit.se/a/topics/computer/terminal/Use-common-commands/index.html)
  * [Praktiska linux kommandon](http://www.htsit.se/a/topics/computer/terminal/Use-more-commands/index.html)
  * _Powershell_

_Tillbaka till [översikt](../index.html)_

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
