# Innehåll
## En introduktion till nätverksteknik
![Linux](../../image/osi.png)

Vi ska lära oss om OS framförallt linux 

## Översikt

  * [Prov](http://htsit.se/e/data-3-network)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * OSI
  * LAN
  * Program för nätverkshantering
  * VPN och VLAN

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
