 Mål från läroplanen
## Om vad som, ska kunnas om nätverksteknik

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Lokala nätverk, uppbyggnad och arbetssätt.
  * Protokoll för dataöverföring via nätverk.
  * Begreppen switchning och routning.
  * Begreppet virtuella nät.


## Kunskapskrav

### E                   

  * Eleven beskriver översiktligt hur datorer och lokala nätverk är uppbyggda och fungerar samt hur driftsäkerhet uppnås.
  * Eleven planerar och utför i samråd med handledare och med visst handlag ... mjukvaruinstallation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i ... datorsystem. 
  * Resultatet är tillfredsställande i fråga om funktion, säkerhet och kvalitet.
  * I arbetet använder eleven med viss säkerhet instruktioner, manualer, topologier och andra dokument på både svenska och engelska.

### C                   

  * Eleven beskriver utförligt hur datorer och lokala nätverk är uppbyggda och fungerar samt hur driftsäkerhet uppnås.
  * Eleven planerar och utför efter samråd med handledare och med gott handlag ... mjukvaruinstallation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i ... datorsystem.

### A                   

  * Eleven beskriver utförligt och nyanserat hur datorer och lokala nätverk är uppbyggda och fungerar samt hur driftsäkerhet uppnås. Dessutom redogör eleven utförligt och nyanserat för sambandet mellan de olika delarna som ingår i nätverkets uppbyggnad.
  * Eleven planerar och utför  … med mycket gott handlag avancerad ... mjukvaruinstallation, uppbyggnad, konfigurering, uppgradering, optimering och felsökning samt åtgärdar fel i ... datorsystem. 
  * Resultatet är gott i fråga om funktion, säkerhet och kvalitet.
  * I arbetet använder eleven med säkerhet instruktioner, manualer, topologier och andra dokument på både svenska och engelska...
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

