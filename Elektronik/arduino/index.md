# Innehåll
## Om Arduino och hur använda elektronik tillsammans med den
![Arduino Uno](../../image/arduino.png)

Vi ska lära oss programmera Arduino och använda den för att kontroller elektronik.

## Översikt

  * [Inlämning](http://htsit.se/e/elmi-3-arduino), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * Intro till programmering
  * Programmera med Scratch
  * Arduino programmering
  * Använd Arduino

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
