# Innehåll
## Om digitatlteknik och några grunder i datorteknik
![Digitalt kretskort](../../image/digital.png)

Vi lär oss digitalteknik med målsättningen att förstå oss på grundläggande datorarkitektur. 

## Översikt

  * [Inlämning](http://htsit.se/e/elmi-4-digital), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  1. Digitalteknik och grindar
  2. Att bygga upp en dator
  3. DAC och ADC

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
