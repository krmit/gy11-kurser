# Mål från läroplanen
## Om digitalteknik och dess tillämpningar

## Kommentarer om läroplannen 


_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Binära talsystemet, boolesk algebra och de digitala grundfunktionerna.
  * Arbetssätt och användningsområden för AD- och DA-omvandlare.
  * Mikrodatorns arbetssätt.
  * ESD-säker (electrostatic discharge) hantering och montering av elektronikkomponenter.


## Kunskapskrav

### E                   

 * Dessutom beskriver eleven översiktligt funktion och arbetssätt hos mikrodatorer och mikroprocessorer.
 * Eleven beskriver översiktligt funktion och egenskaper hos vanligt förekommande digitala ... elektronikkomponenter.
 * Eleven planerar och utför i samråd med handledare montering, hantering och provning av vanligt förekommande digitala... elektronikkomponenter.
 * I arbetet använder och tolkar eleven med viss säkerhet begrepp, symboler, elektronikscheman, instruktioner, manualer, datablad och andra dokument.

### C                   

 * Eleven beskriver utförligt funktion och egenskaper hos vanligt förekommande digitala ... elektronikkomponenter.
 * Eleven planerar och utför efter samråd med handledare montering, hantering och provning av vanligt förekommande digitala ... elektronikkomponenter.

### A                   

 * Eleven beskriver utförligt och nyanserat funktion och egenskaper hos vanligt förekommande digitala … elektronikkomponenter.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

