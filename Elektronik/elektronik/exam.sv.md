# Prov
# Elektronik komponeter
## Innehåll

Inför provet gå igenom lektionsplaneringen och se att du har förstått allt. Gå också igenom frågorna, men tänk på att lära dig svaren utantill inte kommer att räcka för alla frågor. Istället ska du sträva efter att förstå frågorna.

* [Lektionsplanering](https://docs.google.com/document/d/194qx3fJIe1wdx7aeeQ8Gc3DQjC9cPo62sShmlIafIqM/edit?usp=sharing)
* [Frågor](https://htsit.se/e/assignment.html#/el/component)

## Regler

  * Inga hjälpmedel 
  * Du har 60 minuter på dig. Tips: Tiden kan vara avgörande så arbeta snabbt på enkla uppgifter.
  * Provet är uppdelat i tre delar.
    * Del I består av 7 frågor som ger 2 poäng var.
    * Del II består av 2 frågor som ger 3 poäng var.
    * Del III består av 1 frågor som ger 4 poäng.
 * Följande kan ge poängavdrag:
    * Görs en beräkning ska den också redovisas.
    * Vid beräkningar ska rätt enhet och prefix användas
    * Använt fel terminologi
    * Skrivit irrelevanta eller felaktiga påståenden.
  * I Del I så ska svaret vara en fullständig mening eller beräkning om det inte står att du kan besvara frågan på annat sätt.
 * Varje uppgift på Del II och Del III kan innehålla ytterligare regler för hur du får poäng, läs uppgift instruktionen noggrant.

## Bedömning:

Bedömningen beror på totalpoäng, men också på hur väl du klarar de reflekterande frågorna i del II och del III samt om du visat på särskilt goda kunskaper under lektionerna. På ett ungefär kan följande tabell användas men avvikelser i bedömning kan förekomma.

  * __E: 10__
  * __D: 12__ (Var av minst 1 poäng på Del II) 
  * __C: 15__ (Var av minst 2 poäng på Del II) 
  * __B: 18__ (Var av minst 1 poäng på Del III) 
  * __A: 20__ (Var av minst 2 poäng på Del III) 

__Max: 24__

_Tillbaka till [introduktion](./introduktion.html)_

---

**Copyright © 2018, Magnus Kronnäs**

All rights reserved.
