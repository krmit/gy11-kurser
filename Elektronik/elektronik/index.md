# Innehåll
## Sju vanliga komponeter och lite till
![Komponenter](../../image/componentes.png)

Vi kan säga att det finns sju vanliga typer av elektroniki komponeter. Dessa kommer vi gå igenom och några andra komponeter som också kan vara bra att känna till.

## Översikt

  * [Prov](http://htsit.se/e/elmi-2-elektronik)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * De grundläggande analoga komponenterna
  * Uppkopplingar och simuleringar
  * Lödning och ytmontering

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
