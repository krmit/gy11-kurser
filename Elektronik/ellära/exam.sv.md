# Faktaprov om ellära

## Allt om grundläggane ellära

![Ellära](../../image/electricity.png)

## Innehåll

Gå igenom kapitlerna nedan, se till att du förstår innehållet. Gå igenomm varje avsnitt i kapitlet som vi länkar nedan och gör frågorna. Dessa frågor kan komma på provet. Läs också kommentarer till kapitlerna och avsnitten för det kan påverka hur de ska läsas eller vilka frågor som kan tänkas komma.

- [Ohms lag med tillämpningar](http://htsit.se/a/topics/electricity/Basic-circuits/)
  - [En översikt av elektriska kretsar](http://htsit.se/a/topics/electricity/Basic-circuits/00-Overview/)
  - [Serikopplingar](http://www.htsit.se/a/topics/electricity/Basic-circuits/02-Series-circuits/)
  - [Parallellkoppling](http://www.htsit.se/a/topics/electricity/Basic-circuits/03-Parallel-circuits/)
- [Växelström](http://htsit.se/a/topics/electricity/AC)
  - [En översikt av växelström](http://www.htsit.se/a/topics/electricity/AC/00-Overview/)
- [Energi](http://htsit.se/a/topics/electricity/Energy/)
  - [Översikt över Elektrisk energi](http://www.htsit.se/a/topics/electricity/Energy/)
- [Elektromagnetism](http://htsit.se/a/topics/electricity/Energy/)
  - [Översikt över Eletromagnetism](http://www.htsit.se/a/topics/electricity/Electromagnetism/)

## Regler

  * En dator, men du får inte använda internet.
  * Du har 60 minuter på dig. Tips: Tiden kan vara avgörande så arbeta snabbt på enkla uppgifter.
  * Provet är uppdelat i tre delar.
    * Del I består av 7 frågor som ger 2 poäng var.
    * Del II består av 2 frågor som ger 3 poäng var.
    * Del III består av 1 frågor som ger 4 poäng.
 * Följande kan ge poängavdrag:
    * Görs en beräkning ska den också redovisas.
    * Vid beräkningar ska rätt enhet och prefix användas
    * Använt fel terminologi
    * Skrivit irrelevanta påstånden.
  * I del I så ska svaret vara en fullständig mening eller beräkning om det inte står att du kan besvara frågan på annat sätt.
  * Varje uppgift på Del II och Del III kan innehålla ytterligare regler för hur du får poäng, läs uppgift instruktionen noggrant.

## Bedömning:

Bedömningen beror på totalpoäng, men också på hur väl du klarar de reflekterande frågorna i del II och del III samt om du visat på särskilt goda kunskaper under lektionerna. På ett ungefär kan följande tabell användas men avvikelser i bedömning kan förekomma.

  * __E: 10__
  * __D: 12__ (Var av minst 1 poäng på Del II eller Del III) 
  * __C: 14__ (Var av minst 2 poäng på Del II eller Del III) 
  * __B: 17__ (Var av minst 1 poäng på Del III) 
  * __A: 19__ (Var av minst 2 poäng på Del III) 

__Max: 24__


_Tillbaka till [introduktion](./introduktion.html)_

---

**Copyright © 2018, Magnus Kronnäs**

All rights reserved.
