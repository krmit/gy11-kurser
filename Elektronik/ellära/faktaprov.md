# Faktaprov om ellära
## Allt om grundläggane ellära
![Ellära](../../image/electricity.png)

## Innehåll

Gå igenom kapitlerna nedan, se till att du förstår innehållet. Gå igenomm varje avsnitt i kapitlet som vi länkar nedan och gör frågorna. Dessa frågor kan komma på provet. Läs också kommentarer till kapitlerna och avsnitten för det kan påverka hur de ska läsas eller vilka frågor som kan tänkas komma.

  * [Ohms lag med tillämpningar](http://htsit.se/a/topics/electricity/Basic-circuits/)
    * [En översikt av elektriska kretsar](http://htsit.se/a/topics/electricity/Basic-circuits/00-Overview/)
    * [Serikopplingar](http://www.htsit.se/a/topics/electricity/Basic-circuits/02-Series-circuits/)
    * [Parallellkoppling](http://www.htsit.se/a/topics/electricity/Basic-circuits/03-Parallel-circuits/)
  * [Växelström](http://htsit.se/a/topics/electricity/AC)
    * [En översikt av växelström](http://www.htsit.se/a/topics/electricity/AC/00-Overview/)
  * [Energi](http://htsit.se/a/topics/electricity/Energy/)
    * [Översikt över Elektrisk energi](http://www.htsit.se/a/topics/electricity/Energy/)
  * [Elektromagnetism](http://htsit.se/a/topics/electricity/Energy/)
    * [Översikt över Eletromagnetism](http://www.htsit.se/a/topics/electricity/Electromagnetism/)

## Regler
 
  * Inga hjälpmedel.
  * Varje uppgift get två poäng.
  * Ett poäng om svaret är rätt men har brister enligt nedan.
  * För två poäng måste svaret:
    * Var en fullständig mening eller beräkning om det inte står att du kan besvara frågan på annat sätt.
    * Görs en beräkning ska den också redovisas.
    * Vid beräkningar ska rätt enhet och prefix användas.

## Bedömning

Detta prov har en enkel bedöming, men viktiga kunskapskriterier för de högre betygen kommer testats senare i kursen.

__För E:__ 10
__För D:__ 12
__För C:__ 14
__För B:__ 16
__För A:__ 18
__Max:__   20

_Tillbaka till [introduktion](./introduktion.html)_

---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.
