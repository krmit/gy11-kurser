# Innehåll

## Allt om grundläggane ellära

![Ellära](../../image/electricity.png)

Här ska vi lära oss grunderna i ellära.

## Översikt

- [Prov](./exam.sv.html)
- [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

- [Ohms lag med tillämpningar](http://htsit.se/a/topics/electricity/Basic-circuits/)
- [Växelström](http://htsit.se/a/topics/electricity/AC)
- [Energi](http://htsit.se/a/topics/electricity/Energy/)
- [Elektromagnetism](http://htsit.se/a/topics/electricity/Energy/)

_Tillbaka till [översikt](../index.html)_

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
