# Mål från läroplanen
## Att kunna om Ellära och analog elektronik.

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Egenskaper och arbetssätt hos resistorer, kondensatorer, induktanser och transformatorer.
  * Egenskaper och arbetssätt hos analoga och digitala halvledarkomponenter.
  * Schemaritningsteknik, symboler och funktioner.
  * Mätteknik för krets- och komponentmätningar.
  * Datorsimulering av enklare elektronikuppkopplingar.


## Kunskapskrav

### E                   

 * Eleven beskriver översiktligt funktion och egenskaper hos vanligt förekommande analoga ... elektronikkomponenter. 
 * Eleven planerar och utför i samråd med handledare montering, hantering och provning av vanligt förekommande analoga ... elektronikkomponenter.
 * Arbetet utförs på ett sätt som är säkert för eleven själv och andra samt är miljövänligt.
 * … Dessutom utför eleven enkla beräkningar av elektriska storheter.

### C                   

 * Eleven beskriver utförligt funktion och egenskaper hos vanligt förekommande analoga ... elektronikkomponenter. ...
 * Eleven planerar och utför efter samråd med handledare montering, hantering och provning av vanligt förekommande analoga ... elektronikkomponenter.
 * … Dessutom utför eleven beräkningar av elektriska storheter..


### A                   

 * Eleven beskriver utförligt och nyanserat funktion och egenskaper hos vanligt förekommande analoga … elektronikkomponenter.
 * Dessutom utför eleven avancerade beräkningar av elektriska storheter.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

