# Elektronik och mikrodatorteknik

![Mobile](../image/mobile.png)

Detta är en kurs där vi ska lära oss grundläggande elektronik och programmering med Arduino. Eftersom några er ovan med programmering kommer vi göra det kontinuerligt.

## Moment

- [Ellära](./ellära/index.html)
- [Elektronik komponeter](./elektronik/index.html)
- [Arduino](./arduino/index.html)
- [Digitalteknik](./digital/index.html)
- [Mer med Arduino](./more-arduino/index.html)

[**Kursplanen**](../plans/elektronik.pdf)

_Tillbaka till [Kurslista](../kurslista.html)_

---

**Copyright © 2018, Magnus Kronnäs**

All rights reserved.
