# Innehåll
## Om Arduino och hur använda elektronik tillsammans med den
![Arduino Uno](../../image/more-arduino.png)

Vi ska lära oss programmera Arduino och använda den för att kontroller elektronik.

## Översikt

  * [Inlämning](http://htsit.se/e/elmi-5-mer-arduino), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * Motorer
  * Displayer

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
