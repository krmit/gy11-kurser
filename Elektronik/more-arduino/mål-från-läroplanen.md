# Mål från läroplanen
## 

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Mikroprocessorer och deras kringkomponenter.
  * Enklare programmering av mikrodatorer.
  * Förbindningsteknik och ytmonteringsteknik.
  * Egenskaper och arbetssätt hos optokomponenter och displayer.


## Kunskapskrav

### E                   

 * Dessutom beskriver eleven översiktligt funktion och arbetssätt hos mikrodatorer och mikroprocessorer.
 * Eleven planerar och utför i samråd med handledare … enklare programmering av mikrodatorer. 
 * När eleven samråder med handledare bedömer hon eller han med viss säkerhet den egna förmågan och situationens krav.

### C                   

 * Dessutom beskriver eleven utförligt funktion och arbetssätt hos mikrodatorer och mikroprocessorer.
 * Eleven planerar och utför i samråd med handledare … enklare programmering av mikrodatorer. 

### A                   

 * Dessutom beskriver eleven utförligt och nyanserat funktion och arbetssätt hos mikrodatorer och mikroprocessorer.
 * När eleven samråder med handledare bedömer hon eller han med säkerhet den egna förmågan och situationens krav.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

