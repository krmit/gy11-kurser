# Programmering 1

![Javascript logo](../image/javascript_badge.png)

Programmering är ett annat sätt att tänka. Det skiljer sig från matematiken genom att vara dynamisk, en föränderlig process som påverkar sin omgivning.

Vi kommer att lära oss javascript , eftersom de har en syntax som liknar många andra programmeringsspråk. Men vilket programmeringsspråk du lär dig är egentligen inte så intressant. Det viktigaste är att lära sig att tänka som en programmerare.

## Moment

- [Introduktion med Scratch](./introduktion-med-Scratch/index.html)
- [Programmering med js](./programmering-med-js/index.html)
- [Programmerings kultur](./programmerings-kultur/index.html)
- [Problemlösning med js](./problemlösning-med-js/index.html)
- [Eget program](./programmerings-projekt/index.html)

[**Kursplanen**](../plans/programmering.pdf)

_Tillbaka till [Kurslista](../kurslista.html)_

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
