# Innehåll

## Introduktion till programmering och Scratch

![Scratch logo](../../image/scratch-logo.png)

Komma igång med rogrammering med hjälp av Scratch. Detta är en introduktion till programmering med olika förberedande övningar. Vi kommer också att gå igenom grunderna för ett enkelt programmeringsspråk och hur vi kan skapa enkla spel med det språket.

## Översikt

- [Inlämning](./submission.sv.html).
- [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

- [Introduktion till programmering](http://htsit.se/a/topics/programming/Introduction-to-programming/index.html)
- [Programmering med Scratch](http://www.htsit.se/a/topics/programming/scratch/Programing-with-scratch/index.html)
- [Programmera mera med Scratch](http://www.htsit.se/a/topics/programming/scratch/Programing-more-with-scratch/index.html) - Kommer kanske senare
- Exempel
  - [Programmering](https://scratch.mit.edu/studios/4262942/)
  - [Programmera mera](https://scratch.mit.edu/studios/25185486/)
  - [Imponerande Spel](https://scratch.mit.edu/studios/5292443/)
  - [HTSIT bästa](https://scratch.mit.edu/studios/3000432/)

_Tillbaka till [översikt](../index.html)_

---

** Copyright © 2018-2019, Magnus Kronnäs **

All rights reserved.
