# Inlämningsuppgift om Scratch

## Två små program i Scratch

![Scratch logo](../../image/scratch-logo.png)

Du ska göra två scratch program för att visa att du förstår grunderna inom programmering.

## Inlämning

  * Du ska skicka svaret som en e-post till “magnus.kronnas@hassleholm.se”.
  * Du ska också skriva ditt namn i brevet.
  * På redovisningsdagen kommer du få svara på frågor om din inlämning.
  * Se SchoolSoft för deadline.
  * Obs! Det är viktigt att du lämnar in i tid.

Om du inte lämnar in i tid kommer det på SchoolSoft skickas ett meddelande med instruktioner för sen inlämning. Observera att det kommer antas att du fått F på detta moment fram till inlämning har gjorts och du därför också är F varnad.

## Beskrivning

### **Del I:** Gör ett spelet gissa tal

1. Låt användaren gissa ett tal. Om taler är 42 och endast då ska datorn skriva ut “Du gissade rätt”. **(E)**
2. Gör ett program som ska fungera som spelet “Gissa tal”. Datorn utgår ifrån talet 42 är rätt. Användaren får nu gissa ett tal. Om användaren gissade fel säger datorn om talet är större eller lägre och låter användaren gissa igen. I annat fall förklarar datorn att användaren har vunnit och avslutar spelet. **(C)**
3. Förbättra ditt spel ytterligare. Låt datorn slumpa fram ett tal istället och när användaren vinner så ska användaren få veta hur många gissningar hen behövde. **(A)**

### **Del II:** Skapa ett eget spel

#### E nivå:

1. Spelet måste innehålla en uppgift som går att lösa. Denna uppgift kan vara mycket enkel.
2. Det ska gå att vinna i ditt spel.

#### C nivå:

3. Du ska då visa en vinnar animation.
4. Du ska också räkna poäng.
5. När spelaren har vunnit ska poängen visas.
6. Du använder variabler och dessa har namn som visar vad de används till i programmet.

#### A nivå:

7. Ditt spel ska vara “spelbart”, det vill säga underhållande och välgjort.
8. Det ska finnas flera olika banor.
9. Varje bana ska ha olika bakgrund.
10. Sättet att spelet ska skilja sig mellan de olika banorna.
11. Använd minst ett egendefinierade block.
12. Programmet är väl strukturerad så att det är lätt att få en överblick och förstå vad programmet gör.

## Bedömning

Din bedömning kommer bli den lägsta nivån som du får på de olika delarna nedan. Om du klarat en del på en högre nivå än de andra delen kommer ditt betyg bli det lägsta mellanbetyget som är högre än ditt lägsta resultat. Med andra ord om du får E och A, så blir ditt resultat D.

Observera att en bedömning på denna uppgift som visar att ditt resultat är F är också en F-varning. Du kommer då behöva lämna in uppgiften igen för att kunna bli godkänd på kursen. Du får instruktioner för detta via SchoolSoft.

Om du är missnöjd med betyg så kan du också lämna in uppgiften igen. Observera dock att du först ska skicka ett mail till mig så att du kan får instruktioner för hur du ska komplettera. För visa betyg kan också uppgiften förändras när du vill komplettera se kompletterings uppgifterna i slutet av uppgiften.

All diskussion om bedömningar av uppgiften via om e-post, läraren ger inte muntliga svar på detta förutom på resurstiden på fredag mellan 14.30 och 15.30. Sådan brev måste skickas in minst en vecka innan betygen sätts.

## Kompletering

Om du inte bli klar i tid kommer du få följande kompletering:

### För E:

- Gör allt på E nivå på del I och del II.

### För C:

Finns ingen kompletering att göra, kunskaperna går att visa genom att göra C nivå på provet i moment 4.

### För A:

Finns ingen kompletering att göra, kunskaperna går att visa genom att göra A nivå på provet i moment 4.

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
