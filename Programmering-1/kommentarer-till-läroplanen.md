# Kommentarer om progrmmering 1

Ämnesplanen: [Programmering 1](https://www.skolverket.se/laroplaner-amnen-och-kurser/gymnasieutbildning/gymnasieskola/sok-amnen-kurser-och-program/subject.htm?subjectCode=PRR&tos=gy)

_Tillbaka till [översikt](./översikt.html)_

## Kommentarer

Till exempel så ingår inte ett historisk perspektiv i det centrala innehållet, detta perspektiv är ofta väldigt bra för att förklara olika tillämpade aspekter av programmering. Medan det social perspektiv genus som ingår leder till mycket svårt tolkade gränsdragningar av vad som är och inte är en del av kursen.
 
En bra sak med kursen är dock att stora moment i kursen har en tydligt progression. Därför kan en eleven genom att prestera bra på kursdelen 3 och 4 få högsta betyg oavsett vad som har gjort på kursdel 0 och 1.  Däremot är dessa kursdelar givetvis bra förberedelser för senare delarna och om en eleven uppvisar bra resultat på de första delarna kan det förbättra slutbetyget, särskilt för de lägre betygsstegen. Del 2 innehåller däremot många moment som är unika för denna del vilket ett högt betyg hör bli avgörande för slutbetyget. Men du kan alltid förbättra ditt resultat på del 2,3 och 4 senare i kursen om du vill. Del 4 kommer antagligen uppfattas som den svåraste delen och för ambitiösa elever kommer det finnas möjlighet att börja arbeta på denna del tidigare. 

---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.
