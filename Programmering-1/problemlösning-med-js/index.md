# Innehåll
## Vi ska lära oss lösa problem med js
![Problem](../../image/problem.png)

Vi ska se om vi lärt oss grunderna i programmering genom att föska lösa några kortare programmeringsproblem ofta inspiretat av matematik. Problemmen har olika svårighetsgrader.

Mesta tiden i detta moment kommer gå åt till att lösa programmeringsuppgifter.

## Översikt

  * [Prov](http://htsit.se/e/prog1-4-proplem)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * Programmera med JS(repetion)
  * Hur löser man det?
  * Algoritmer och datastrukturer.
  * Felsökning
  * Exempeluppgifter


_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
