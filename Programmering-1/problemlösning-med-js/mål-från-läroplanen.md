# Mål från läroplanen
## Krav som vi strävar mot i programmering

## Kommentarer om läroplannen 

_ **At göra:** Visa av dessa delar är egentligen vara på moment 2._

_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Grundläggande programmering i ett eller flera programspråk varav minst ett av språken är textbaserat.
  * Grundläggande kontrollstrukturer, konstruktioner och datatyper.
  * Gränssnitt för interaktion mellan program och användare.
  * Normer och värden inom programmering, till exempel läsbarhet, dokumentation, testbarhet, rena gränssnitt och nyttan av standard.
  * Strukturerat arbetssätt för problemlösning och programmering.
  * Arbetsmetoder för förebyggande av programmeringsfel, testning, felsökning och rättning av kod.
  * Grundläggande datastrukturer och algoritmer.



## Kunskapskrav

### E                   
  * Eleven formulerar och planerar i samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med viss säkerhet kontrollstrukturer, metoder, variabler, datastrukturer ... som är adekvata för uppgiften.              
  * Eleven redogör översiktligt för principer för att uppnå god kvalitet vid skapandet av datorprogram.              
  * Eleven kommunicerar om programmeringsuppgiften och dess utvärdering och använder då med viss säkerhet datavetenskapliga begrepp.
  * Eleven formulerar och planerar i samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med viss säkerhet kontrollstrukturer, …, variabler, datastrukturer och algoritmer som är adekvata för uppgiften.
  * Eleven utför felsökning av enkla syntaxfel.                   
  * I sin programmering skriver eleven, med konsekvent kodningsstil och tydlig namngivning, korrekt, strukturerad och enkelt kommenterad källkod med tillfredsställande resultat.    
  * Elevens färdiga program eller skript är utförda med tillfredsställande resultat i ett eller flera programspråk som är stabila och robusta i program av enkel karaktär.
  * Eleven anpassar med viss säkerhet sin planering av programmeringsuppgiften.

### C
  * Eleven formulerar och planerar i efter samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med viss säkerhet kontrollstrukturer, metoder, variabler, datastrukturer ... som är adekvata för uppgiften.              
  * Eleven redogör utförligt för principer för att uppnå god kvalitet vid skapandet av datorprogram.
  * Eleven formulerar och planerar i efter samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med viss säkerhet kontrollstrukturer, …, variabler, datastrukturer och algoritmer som är adekvata för uppgiften.
  * Eleven utför felsökning på ett systematiskt och effektivt sätt syntaxfel, körtidsfel och programmeringslogiska fel.
  * I sin programmering skriver eleven, med konsekvent kodningsstil och tydlig namngivning, korrekt, strukturerad och noggrant kommenterad källkod med tillfredsställande resultat.    
  * Elevens färdiga program eller skript är utförda med tillfredsställande resultat i ett eller flera programspråk som är stabila och robusta.  

### A
  * Eleven formulerar och planerar i efter samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med säkerhet kontrollstrukturer, metoder, variabler, datastrukturer ... som är adekvata för uppgiften samt motiverar utförligt sina val.              
  * Eleven redogör utförligt och nyanserad för principer för att uppnå god kvalitet vid skapandet av datorprogram.              
  * Eleven kommunicerar om programmeringsuppgiften och dess utvärdering och använder då med säkerhet datavetenskapliga begrepp.
  * Eleven formulerar och planerar i efter samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med säkerhet kontrollstrukturer, …, variabler, datastrukturer och algoritmer som är adekvata för uppgiften.
  * Eleven utför felsökning på ett systematiskt sätt syntaxfel, körtidsfel och programmeringslogiska fel.               
  * I sin programmering skriver eleven, med konsekvent kodningsstil och tydlig namngivning, korrekt, strukturerad och noggrant och utförligt kommenterad källkod med gott resultat.    
  * Elevens färdiga program eller skript är utförda med gott resultat i ett eller flera programspråk som är stabila och robusta i program av komplex karaktär.
  * Eleven anpassar med säkerhet sin planering av programmeringsuppgiften.           
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

