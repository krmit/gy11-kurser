# Innehåll
## Grundläggande programmering med js.
![Programmering](../../image/program.png)

Vi går långsamt igenom javascript och göra olika enkla övningar för att lära oss ett programmerings språk med en svårare syntax än Scratch.

## Översikt
  * [Prov](http://htsit.se/e/prog1-2-intro-js)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * [Introduktion till JS](http://htsit.se/a/topics/programming/js/Introduction-to-js/)
  * [JS med ett litet spel](http://htsit.se/r)
  * [Programmera med JS](http://htsit.se/a/topics/programming/js/Programing-with-js/)
    * [Test på JS programmering](./test.html)
  * Använda JS i HTML
  * _Programmera mera med JS_
  * _Exempel på roliga JS ramverk_

## Guider

  * Skriv och testa ett enkelt JS program
  * Kom igång med robotspelet.
  * _Kom igång med HTML och DOM._

_Tillbaka till [översikt](../index.html)_

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
