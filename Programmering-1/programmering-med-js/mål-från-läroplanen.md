# Mål från läroplanen
## 

## Kommentarer om läroplannen 

Observera att vi i denna kursdel bara strävar mot att uppnå kunskapsmålen. Ett omdöme här ska ses som en prognos på vad ett betyg kan tänkas bli, se lärar kommentaren till kursen.

_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Gränssnitt för interaktion mellan program och användare.
  * Normer och värden inom programmering, till exempel läsbarhet, dokumentation, testbarhet, rena gränssnitt och nyttan av standard.
  * Strukturerat arbetssätt för problemlösning och programmering.
  * Arbetsmetoder för förebyggande av programmeringsfel, testning, felsökning och rättning av kod.
  * Grundläggande datastrukturer och algoritmer.

## Kunskapskrav

### E                   

 * Eleven redogör översiktligt för principer för att uppnå god kvalitet vid skapandet av datorprogram.              
 * Eleven kommunicerar om programmeringsuppgiften och dess utvärdering och använder då med viss säkerhet datavetenskapliga begrepp.
 * Eleven utför felsökning av enkla syntaxfel.                   
 * I sin programmering skriver eleven, med konsekvent kodningsstil och tydlig namngivning, korrekt, strukturerad och enkelt kommenterad källkod med tillfredsställande resultat

### C                   

 * Eleven redogör utförligt för principer för att uppnå god kvalitet vid skapandet av datorprogram.
 * Eleven utför felsökning på ett systematiskt och effektivt sätt syntaxfel, körtidsfel och programmeringslogiska fel.               
 * I sin programmering skriver eleven, med konsekvent kodningsstil och tydlig namngivning, korrekt, strukturerad och noggrant kommenterad källkod med tillfredsställande resultat.

### A                   

 * Eleven redogör utförligt och nyanserad för principer för att uppnå god kvalitet vid skapandet av datorprogram.              
 * Eleven kommunicerar om programmeringsuppgiften och dess utvärdering och använder då med säkerhet datavetenskapliga begrepp.
 * Eleven utför felsökning på ett systematiskt sätt syntaxfel, körtidsfel och programmeringslogiska fel.               
 * I sin programmering skriver eleven, med konsekvent kodningsstil och tydlig namngivning, korrekt, strukturerad och noggrant och utförligt kommenterad källkod med gott resultat. 
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

