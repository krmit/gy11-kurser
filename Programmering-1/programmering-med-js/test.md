# Test i programmering
## Komplettera några uppgifter med Javascript
![Programmering](../../image/program.png)

Du får tre uppgifter plus en bonus uppgift. Lös dessa uppgifter i enligthet med instruktioner.

_Tillbaka till [introduktion](./introduktion.html)_

## Innehåll

Gå igenom kapitlet om "Programmera med JS" men bara fram till avsnittet om loopar nummer 10.
  * [Programmera med JavaScript](http://htsit.se/a/topics/programming/js/Programing-with-js/)
    * De tio första avsnitetn.

Gå igenomm uppfiterna som du hittar i komprimerad mapp med uppgifter.
  * [Komprimerad map med uppgifter](http://htsit.se/f/ex.zip)

## Regler

  * Du får fyra poäng per uppgift.
    * Ett poäng om du har börjat på en lösning.
    * Ett poäng till om koden fungerar utan syntax error.
    * Ett poäng till om koden ger ett korrekt svar.
    * Ett poäng till om koden följer övriga krav för programmeirngskod.
  * Du får inte använda internet.
  
  
### För alla programmeringskod gäller

  * Alla variabler har namn som visar vad de används till i programmet.
  * Koden på uppgifterna är kort och lätt överskådlig.
  * Du ska undvika att sprida din kod innan den har blivit bedömd.


## Bedömning

Detta är ett test och jag kommer vanligtbiss inte ta hänsyn till det i betygsättning.

 * __För E:__  5
 * __För D:__  6
 * __För C:__  8
 * __För B:__ 10
 * __För A:__ 12
 * __Max:__   12+4

## Kompletteringar

Detta är ett test och du kan inte kompletera det.

---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.
