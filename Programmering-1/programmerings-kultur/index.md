# Innehåll
## Om diverse olika saker som berör programmering på ett eller annat sätt
![Lovelace](../../image/lovelace.png)

Olika datorkomponeter och hur vi använder dem.

## Översikt

  * [Inlämning](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * Datorn och programmeringens historia
  * Hur fungerar ett program.
  * Olika programmeringsspråk
  * Programmeringens användningsområden.
  * Programmeringsingskultur
  * Olika sociala perspektiv

## Guider

  * _Hur du skriver en analyaserande text_

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
