# Mål från läroplanen
## Det svår tolkade delarna av kursen

## Kommentarer om läroplannen 

Detta är definit de mest svårt bedömda och omdiskuterade delen av kursplen. Det "socialt perspektiv" passar helt inte in i ämnets syfter och det är svårt att se hur det på någon meningsfullt sätt ska kunna integreras med ubildningen. Faktum är att det verkar färkomma att dessa delar helt ignoreras. Det är givetviss felaktigt tillväga gångsätt, det är elverna och lärarens uppgift att följa skolverkets styrdokument även om skolverket instruktioner förvirrade och omöjliga att förstå.

_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Programmering och dess olika användningsområden ur ett socialt perspektiv inklusive genus, kultur och socioekonomisk bakgrund.
  * Programmeringens möjligheter och begränsningar utifrån datorns funktionssätt.

## Kunskapskrav

### E                   

 * Eleven väljer med viss säkerhet ett uttryckssätt som är anpassat för att på ett tillfredsställande sätt interagera med den avsedda användaren.                   
 * Eleven redogör översiktligt för programmeringens möjligheter och begränsningar samt hur programmering har påverkat och påverkar vardagen.

### C                   

 * Eleven redogör utförligt  för programmeringens möjligheter och begränsningar samt hur programmering har påverkat och påverkar vardagen.

### A                   

 * Eleven väljer med säkerhet ett uttryckssätt som är anpassat för att på ett gott sätt interagera med den avsedda användaren.                   
 * Eleven redogör utförligt och nyanserat för programmeringens möjligheter och begränsningar samt hur programmering har påverkat och påverkar vardagen.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

	
