# Programmeringskultur
## Skriv en kort analys av ett socialt perspektiv på programmering
![Lovelace](../../image/lovelace.png)

Du ska skriva en essä liknande text för att träna och visa din förmåga att analysera olika social perspektiv på programmering.

Det viktigaste i denna uppgift är att du skapar din egen text och reflekterar över den utifrån ämnet. Skriv en text med rubrik som är inom ämnet  “Socialt perspektiv på programmering”. Du kan välja helt själv hur detta ska vara upplagt, men försöka göra det så att du tycker det är kul. Du ska följa kraven som finns nedan under rubriken “Krav” och ditt arbete blir bedömt enligt det som stör under rubriken “Bedömning”. Du kan använda dig av det som står under “Tips”, men det är en hjälp till dig och inga regler som måste följas.

Observera att det är inte någon svår uppgift! Skriv bara på och det kommer gå bra.

## Krav

  * Du ska bara skriva på svenska, såvida det inte är en av idéen bakom din essä.
  * Ditt språk kommer inte bedömas förutom att du ska använder datavetenskaplig terminologin på rätt sätt. Men för din egen skull är det roligare om du försöker få språket så bra som möjligt. I annat fall kanske du bli gymnasielärare ;)
  * Med datavetenskaplig terminologi menas antingen terminologi i överensstämmelse med standard eller vanlig användning av orden inom aktuell bransch.
  * Inga texter som inte är mellan 400-900 ord kommer bedömas.
  * Texten ska ha en rubrik.

## Tips

  * Tänk igenom din text som en helhet från frågeställning till slutsats innan du börjar skriva.
  * Välj ett ämne och en frågeställning som du tycker är intressanta.
  * Gör ofta intressanta referenser till andra verk.
  * "Meta" är alltid kul, kritisera gärna denna uppgift i din essä :)
  * Var genialisk! Försök! Det gör inget om din text faller platt eftersom du försöker för mycket, i detta sammanhang är det bara roligt.
  * Frågeställningen bör vara öppen, dvs den ska ha många svar och inbjuda till analys.
  * Ditt språk bör vara personligt, men samtidigt korrekt.
  * Läs igenom din text många gånger och korrigera fel. Använd stavnings och grammatik hjälp. Du får gärna fråga någon om hjälp med att hitta språkliga fel eller för att diskutera din idé med, men inte mer än så för det ska vara din egen text.
  * Se till att du har en bra presentation av ditt ämne i börja av din text.
  * Du bör vara öppen i din argumentation, försök inte dölja svagheter i ditt resonemang utan analysera dem istället.
  * Ha en slutsatts som är svar på frågeställningen, men också gärna en intressant slutkläm.

## Förslag på ämnen
Obs! Inget av det som står nedan är nödvändig sant eller falsk. Det är bara förslag på något man kan reflektera kring.

  * Regeln att elever inte har tillgång till admin kontot på sin skoldatorer missgynnar elever med dålig socioekonomiska bakgrund.
  * Svårigheten att kombinera familjeliv med påstådda långa arbetsdagar skrämmer bort kvinnor från IT-yrken.
  * De nya kursplanernas krav på analyser utifrån ett social perspektiv gör  kursen svårare för elever med sämre språklig förmåga och motverkar därför sitt syfte.
  * De många skildringar av våld och sex i spel gör dagens ungdomar mer avtrubbade vilket har många negativa effekter på deras social förmåga.

## Bedömning

### Nivå E:

  * Din frågeställning är relevant utifrån “ett socialt perspektiv inklusive genus, kultur och socioekonomisk bakgrund.”
  * Du använder flera datavetenskapliga termer.
  * Med lite god vilja kan man hitta en röd tråd i din text.
  * Texten är på  minst 300 ord.
  * Texten har ett klart avslut.

### Nivå C:

  * Din frågeställning är intressant.
  * Du använder på ett korrekt sätt datavetenskapliga termer.
  * Texten är på  minst 400 ord.
  * Texten har en tydlig röd tråd.

### Nivå A:

  * Din text som helhet intresseväckande.
  * Du använder datavetenskapliga termer ofta i din text och med stor skicklighet.
  * Texten är lagom lång utifrån din idé.
  * Du har ett tydligt och argumenterande resonemang.
