# Innehåll
## Gör ditt eget program
![Mitt program](../../image/mitt-program.png)

Skapa ditt eget program.

## Översikt

  * [Inlämning](http://htsit.se/e/prog1-5-program), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel


## Guider



_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
