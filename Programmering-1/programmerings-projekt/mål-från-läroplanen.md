# Mål från läroplanen
## Programmering 1

## Kommentarer om läroplannen 

Observera att vi i denna kursdel bara strävar mot att uppnå kunskapsmålen. Ett omdöme här ska ses som en prognos på vad ett betyg kan tänkas bli, se lärar kommentaren till kursen.

_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Grundläggande programmering i ett eller flera programspråk varav minst ett av språken är textbaserat.
  * Grundläggande kontrollstrukturer, konstruktioner och datatyper.

## Kunskapskrav

### E                   
 * Eleven formulerar och planerar i samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med viss säkerhet kontrollstrukturer, metoder, variabler, datastrukturer och algoritmer som är adekvata för uppgiften.

 * Elevens färdiga program eller skript är utförda med tillfredsställande resultat i ett eller flera programspråk som är stabila och robusta i program av enkel karaktär.
Eleven anpassar med viss säkerhet sin planering av programmeringsuppgiften.

### C                   

 * Eleven formulerar och planerar i efter samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med viss säkerhet kontrollstrukturer, metoder, variabler, datastrukturer och algoritmer som är adekvata för uppgiften.
 * Elevens färdiga program eller skript är utförda med tillfredsställande resultat i ett eller flera programspråk som är stabila och robusta.

### A                   
 * Eleven formulerar och planerar i efter samråd med handledare programmeringsuppgifter med pseudokod eller diagramteknik. I planeringen väljer eleven med säkerhet kontrollstrukturer, metoder, variabler, datastrukturer och algoritmer som är adekvata för uppgiften.
 * Elevens färdiga program eller skript är utförda med gott resultat i ett eller flera programspråk som är stabila och robusta i program av komplex karaktär.
 * Eleven anpassar med  säkerhet sin planering av programmeringsuppgiften.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

