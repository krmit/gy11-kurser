# Programmera fritt

Du ska göra två scratch program för att visa att du förstår grunderna inom programmering.

## Bedömning

Observera att denna kurs har stor progression, det vill säga delar i slutet av kursen bygger vidare på vad som görs i början av den. 

Det gör att bedömningen på denna uppgift är mer som en gissning om hur det kan gå inte något annat. Bara om du ligger på gränsen till F i slutet av kursen kommer jag ta hänsyn till ditt resultat här och i så fall använda det som en komplettering. Men det är en mycket bra träning på vad som komma skall. 

Din bedömning kommer bli den lägsta nivån som du klara på de olika delarna nedan. Eller om du klarat en del på en högre nivå än den andra delen. Kommer ditt betyg bli det lägsta mellanbetyg som är högre än ditt lägsta resultat. Med andra ord om du får E och A på de olika delarna, så blir ditt resultat D.

### Skapa ett eget spel

#### E nivå:

#### C nivå:

#### A nivå:


## Kompletering

Om du inte bli klar i tid kommer du få följande kompletering:

### För E:  
  * Gör allt på E nivå på del I och del II.

### För C:
  Finns ingen kompletering att göra, kunskaperna går att visa genom att göra C nivå på provet i moment 4.

### För A:
  Finns ingen kompletering att göra, kunskaperna går att visa genom att göra A nivå på provet i moment 4.

---

__Copyright © 2018-2019, Magnus Kronnäs__

All rights reserved.
