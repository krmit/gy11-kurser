# Innehåll
## Allt om js
![You Don't Know JS](../../image/you-dont-know-JS.png)

Vi ska här lära oss allt om JS. Detta kapitel är inspirerat av boken [You Dont Know JS](https://github.com/getify/You-Dont-Know-JS) 

## Översikt

  * [Inlämning](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * [Datatypes](http://htsit.se/a/topics/programming/js/Datatypes/index.html)
  * [Scope, Closures och Variabler]
  * [This and Prototypes]()
  * [Asynchronous kod och promises]()

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
