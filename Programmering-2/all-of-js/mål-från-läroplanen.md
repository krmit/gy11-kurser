# Mål från läroplanen
## Allt om JS

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Variablers och metoders synlighet och livslängd.
  * Stark och svag samt statisk och dynamisk typning.
  * Identifierares synlighet och livslängd.
  * Det valda programspråkets kontrollstrukturer.

## Kunskapskrav

### E                   

 * I sin programmering skriver eleven med konsekvent kodningsstil och tydlig namngivning en korrekt, strukturerad och enkelt kommenterad källkod med tillfredsställande resultat.

### C                   

 * I sin programmering skriver eleven med konsekvent kodningsstil och tydlig namngivning en korrekt, strukturerad och noggrant kommenterad källkod med tillfredsställande resultat.

### A                   

 * I sin programmering skriver eleven med konsekvent kodningsstil och tydlig namngivning en korrekt, strukturerad och noggrant och utförligt kommenterad källkod med gott resultat.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

