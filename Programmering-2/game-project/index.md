# Innehåll
## Skap ditt eget spel
![Spel](../../image/simple-game.png)

Vi ska hjälpas åt att skapa ett spel.

## Översikt

  * [Inlämning](http://htsit.se/e/prog2-5-game), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * HTSIT modellen för projektarbete


_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
