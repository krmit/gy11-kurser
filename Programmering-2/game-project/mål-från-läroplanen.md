# Mål från läroplanen
## Om att arbeta i projektform med visa kunsakpskrav.

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Skapande av klasser och objekt i ett objektorienterat programspråk utifrån tidigare analys och design.
  * Användning av klasser och att genom arv förändra beteende hos klasser som ingår i egna och andras klasshierarkier och standardbibliotek.
  * Analys, nedbrytning och modellering av programmeringstekniska problem med lämpligt analysverktyg, till exempel användningsfall.
  * Skapande av användarvänliga gränssnitt.
  * Skrivning och läsning av lagrad data.
  * Utveckling av program som nyttjar kommunikation över internet.


## Kunskapskrav

### E                   

 * Eleven analyserar och modellerar enklare uppgifter i samråd med handledare.
 * Eleven väljer med viss säkerhet ett uttryckssätt och ett gränssnitt som är anpassat för att på ett tillfredsställande sätt interagera med den avsedda användaren. 
 * Eleven skapar någon enkel lösning där programmet kommunicerar över internet.
 * Eleven anpassar med viss säkerhet sin planering av programutvecklingsprocessen och utför felsökning av enkla syntaxfel.
 * Innan programutvecklingen avslutas utvärderar eleven med enkla omdömen programmets prestanda och ändamålsenlighet i någon situation och i något sammanhang.

### C                   

 * Eleven analyserar och modellerar uppgifter efter samråd med handledare.
 * Eleven skapar någon lösning där programmet kommunicerar över internet.
 * Eleven anpassar med viss säkerhet sin planering av programutvecklingsprocessen och utför på ett systematiskt sätt felsökning av syntaxfel, körtidsfel och programmeringslogiska fel. 
 * Innan programutvecklingen avslutas utvärderar eleven med nyanserade omdömen programmets prestanda och ändamålsenlighet i några situationer och sammanhang. 


### A
       
 * Eleven analyserar och modellerar komplexa uppgifter efter samråd med handledare. 
 * Eleven väljer med säkerhet ett uttryckssätt och ett gränssnitt som är anpassat för att på ett gott sätt interagera med den avsedda användaren. 
 * Eleven skapar någon genomarbetad lösning där programmet kommunicerar över internet.
 * Eleven anpassar med säkerhet sin planering av programutvecklingsprocessen, anpassar polymorfi och utför på ett systematiskt och effektivt sätt felsökning av syntaxfel, körtidsfel och programmeringslogiska fel. 
 * Innan programutvecklingen avslutas utvärderar eleven med nyanserade omdömen och ger förslag på förbättringar av programmets prestanda och ändamålsenlighet i flera situationer och sammanhang. 
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

