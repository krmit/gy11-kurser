# Skapa ditt eget spel.

Skapa ditt eget spel.


## Bedömning

## Bedömning



## __Del I:__ Gör ett spelet gissa tal

  1. 

## __Del II:__  Skapa ett eget spel

### E nivå:

  1. 

### C nivå:

  2. 

### A nivå:

  3. 

## Kompletering

Om du inte bli klar i tid kommer du få följande kompletering:

### För E:  

  * Gör allt på E nivå på del I och del II.
  * __Slutprojekt:__ 

### För C:

  * 

### För A:

  * 
  
---

__Copyright © 2018-2019, Magnus Kronnäs__

All rights reserved.
