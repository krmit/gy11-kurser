# Programmering 2

![Programera mera](../image/programming-js.png)

Programmering 2 handlar om att lära os javascript på djupet och hur vi använder OO med hjälp av typescript. Du ska också lära dig att skapa spel. Med hjälp av js och olika hjälpmedel ska vi lära oss om avancerad programmering.

## Moment

- [Repetion av js](./repetition-of-js/index.html)
- [Allt om js](./all-of-js/index.html)
- [Typescript och object orientering](./typescript-and-oo/index.html)
- [Spelprogrammering med Phaser](./phaser/index.html)
- [Eget spel projekt](./game-project/index.html)

[**Kursplanen**](./plans/programmering.pdf)

_Tillbaka till [Kurslista](../kurslista.html)_

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
