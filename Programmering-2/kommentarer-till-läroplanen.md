# Kommentarer om Programmering 2

Ämnesplanen: [Programmering 2](https://www.skolverket.se/laroplaner-amnen-och-kurser/gymnasieutbildning/gymnasieskola/sok-amnen-kurser-och-program/subject.htm?subjectCode=PRR&tos=gy)

_Tillbaka till [översikt](./översikt.html)_

## Kommentarer

Kursplan för denna kurs är motsägelsefull och tycks inte var riktig genomtänkt. Till exempel har vi inget centralt innehåll som berör kunskap om olika programmeringsspråk och deras egenskap, trots att det både är ett mål med kursen och det finns kunskapskrav som berör det. Utifrån ett av kunskapskrav verkar det som elever får välja programmeringsspråk fritt vilket skulle var mycket svårt att genomföra i praktiken. Därför finns kursdel 0 som ger en fördjupat diskussion om olika programmeringsspråk och det avslutande projektet kan utifrån vissa krav skrivas i vilket programmeringsspråk som helst förutsatt att vissa krav uppfylls.

---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.
