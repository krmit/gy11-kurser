# Innehåll
## Lära oss phaser som ett exempel på ett API
![Phaser](../../image/phaser.png)

Lära sig phaser som ett exempel på ett API

## Översikt

  * [Prov](http://htsit.se/e/prog2-4-phaser)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * Introduktion till Phaser
  * Phaser olika delar
  * Exempel spel
  * Filsystem
  * Använda yml
  * Att använder internet för kommunikation
  * Användargränssnitt med HTML


_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
