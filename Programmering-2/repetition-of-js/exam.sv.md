# Inför prov om js

## Repetion av javascript

![Repetion](../../image/repetition.png)

## Innehåll

Se den korta planeringen av [lektionerna](https://docs.google.com/document/d/12CRmx0UNn_hQy9rWg1V0m2-8mEpdtsWW9BtMLJsyGxo/edit?usp=sharing). Den har alla relevanta länkar.

## Regler

  * En dator, men du får inte använda internet.
  * Du har 60 minuter på dig. Tips: Tiden kan vara avgörande så arbeta snabbt på enkla uppgifter.
  * Provet är uppdelat i tre delar.
    * Del I består av 7 frågor som ger 2 poäng var.
    * Del II består av 2 frågor som ger 3 poäng var.
    * Del III består av 1 frågor som ger 4 poäng.
 * Följande kan ge poängavdrag:
    * Inte skrivit logg i enlighet med instruktionernerna.
    * Uppenbart felaktiga radnummer för dina kodändringar.
    * Onödiga ändringar av koden.
    * Inga kommentarer om du göra stora ändringar till koden.
* Varje uppgift på Del II och Del III kan innehålla ytterligare regler för hur du får poäng, läs uppgift instruktionen noggrant.

## Bedömning:

Bedömningen beror på totalpoäng, men också på hur väl du klarar de reflekterande frågorna i del II och del III samt om du visat på särskilt goda kunskaper under lektionerna. På ett ungefär kan följande tabell användas men avvikelser i bedömning kan förekomma.

  * __E: 10__
  * __D: 12__ (Var av minst 1 poäng på Del II och Del III) 
  * __C: 14__ (Var av minst 2 poäng på Del II och Del III) 
  * __B: 17__ (Var av minst 1 poäng på Del III) 
  * __A: 19__ (Var av minst 2 poäng på Del III) 

__Max: 24__


_Tillbaka till [introduktion](./introduktion.html)_

---

**Copyright © 2019, Magnus Kronnäs**

All rights reserved.
