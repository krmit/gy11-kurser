# Inlämninguppgift i js

## Repetion av javascript

![Repetion](../../image/repetition.png)

**✋ Uppgiften inte klar ännu**

En repetion av js.

## Bedömning

I

#### A

- I

#### C

- I

#### A

- I

## Kompletering

### E

- G

### C

- G

### A

- G

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
