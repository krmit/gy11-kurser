# Innehåll
## Lära oss Object Orientering med HTSIT Advenmtures och Typescript
![Typescript](../../image/typescript.png)

Du ska lära dig grunderna om object orientering och hur man gör en design. Du kan läsa källkoden för projektet som vi diskuterar som finns [här](https://gitlab.com/krmit/mimer-adventure-pre) och du kan också läsa min lektions anteckningar om OO som du hittar [här](https://docs.google.com/document/d/1ImtVk4STtwKR67-cX9SD2H72H_gxnhBWu4NppVw-6V0/edit?usp=sharing).

Objed orientering är en populät design mönster. Vi kan använda JS variaten typescript för at enkelt kunna använda det.

## Översikt
  * [Prov](http://htsit.se/e/prog2-3-oo)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * Dialekter och versioner av JS
  * Introduktion till Typescript
  * Klasser i Typescript
  * Arv
  * Polymorphism
  * UML

_Tillbaka till [översikt](../index.html)_

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
