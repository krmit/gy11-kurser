# Mål från läroplanen
## Mål med att lära sig objec orientering

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Arv, inkapsling och polymorfism.
  * Generiska klasser och metoder.
  * Design av lämplig lösning ur föregående analys med lämpligt verktyg och metoder som klassdiagram.
  * Grunderna för klasserna, objekt, egenskaper och metoder.
  * Undantagshantering.



## Kunskapskrav

### E                   

 * Eleven skapar med viss säkerhet enklare program med klasser där arv används i begränsad utsträckning.
 * Produkten är av tillfredsställande kvalitet i ett eller flera programspråk och innehåller objektorienterad programmering som är stabil och robust i program av enkel karaktär.
 * Eleven kommunicerar om programutvecklingen och dess utvärdering och använder då med viss säkerhet datavetenskapliga begrepp.
 * Eleven designar med viss säkerhet utifrån den analyserade uppgiften en enkel lösning och dokumenterar denna med ett enkelt klassdiagram.
 * Eleven använder med viss säkerhet någon generisk klass.
 * Eleven formulerar och planerar i samråd med handledare programmeringsuppgifter och väljer med viss säkerhet lämpliga programspråk.

### C                   

 * Eleven skapar med viss säkerhet enklare program med klasser där arv används.
 * Produkten är av tillfredsställande kvalitet i ett eller flera programspråk och innehåller objektorienterad programmering som är stabil och robust.
 * Eleven designar med viss säkerhet utifrån den analyserade uppgiften en lösning och dokumenterar denna med ett klassdiagram. 
 * Eleven använder med viss säkerhet några generiska klasser. 
 * Eleven formulerar och planerar efter samråd med handledare programmeringsuppgifter och väljer med viss säkerhet lämpliga programspråk.

### A                   

 * Eleven skapar med säkerhet enklare program med klasser där arv används i omfattande utsträckning.
 * Produkten är av god kvalitet i ett eller flera programspråk och innehåller objektorienterad programmering som är stabil och robust i program av komplex karaktär.
 * Eleven kommunicerar om programutvecklingen och dess utvärdering och använder då med säkerhet datavetenskapliga begrepp.
 * Eleven designar med säkerhet utifrån den analyserade uppgiften en komplex lösning och dokumenterar denna med ett avancerat klassdiagram.
 * Eleven använder med säkerhet flera generiska klasser. 
 * Eleven formulerar och planerar efter samråd med handledare programmeringsuppgifter och väljer med säkerhet lämpliga programspråk.    
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

