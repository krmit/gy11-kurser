# HTSIT Advenmtures och Typescript

Du ska lära dig grunderna om object orientering och hur man gör en design.

## Bedömning

Din bedömning kommer bli den lägsta nivån som du klara nedan. Eller om du klarat en del på en högre nivå än den andra delar kommer ditt betyg bli det lägsta mellanbetyg som är högre än ditt lägsta resultat. Med andra ord om du får E och A, så blir ditt resultat D.

Observera att en bedömning som visar att ditt resultat är F är också en F-varning. du kommer då behöva göra komplettering för minst E nivå.

## __Del I:__ Gör ett antal klasser utfrån uppgifter

Gör uppgifterna nedan. Du kan lösa en uppgift mer än en gång, men beskriv varje uppgift tydlig om du lämnar in mer än en. Du kan skicka in uppgifterna kontinuerligt om du vill. De inlämningar som passar bäst in i spelet kommer att införas permanent i arkivet om du vill, men observera att de kan tas bort i framtiden vid behov. 

Hämta hem filerna nedan och gör uppgifterna. För att hämta hem dem kan du göra följande:

  * Titta på kälkkoden.
  * Ctrl-A, Ctrl-C för att kopiera koden.
  * Klistra in i en html fil. 

På Unix/Linux använd kommandot: *wget http://htsit.se/p/exercise-X.html* - Där X är numret på uppgiften.

Tips: Vi uppdateringar av projektet kan det ibland bli fel eftersom din webbläsare har en gammal version js bibloteken som är en del av projektet. För aatt tas bort catch under utveckling i Firefox gör på följande vis:

1. Tryck F12 för att öppna dev konsolen.
2. Tryck F1 för att komma fram till "Settings".
3. Gå till "Advanced Settings"
4. Markera "Disable HTTP Cache ..." 

###  1. [Uppgift 1](http://htsit.se/p/exercise-1.html) - Individ utifrån de klasser som finns definierade.

  * **(E)** Skapa en klass som genom att ärva från någon “creature” klass.
  * **(E)** Använd en mix-in klass från “type” mappen. **Eller**
  * **(E)** Kommentera i koden vad som menas med  “Common” i namnet på visa individ klasser.

###  2. [Uppgift 2](http://htsit.se/p/exercise-2.html) - Skapa en egen “creature” klass, dvs en klass som ärver ifrån creature.

  * **(E)** Klassen baserat på “Human” klassen. **Eller**
  * **(C)** Klassen baserat på “Humanoid” klassen. **Eller**
  * **(A)** Klassen baserat på “Creature” klassen.

###  3. [Uppgift 3](http://htsit.se/p/exercise-3.html) - Skapa en egen “type” klass, dvs en mix-in klass som kan användas på creature klasser.

  * **(E)** Klassen innehåller förändringar av någon medlem. **Eller**
  * **(C)** Klassen innehåller en ny attack metod **Eller**
  * **(A)** Klassen kompletterar på ett bra sätt andra klasser, t.ex. genom att addera en kostand för typen till "cost".

###  4. [Uppgift 4](http://htsit.se/p/exercise-4.html) - Skapa en individ klass som är definierad utifrån uppgift 2 och 3.
  * **(E)** Individen fungerar i spelet.
  * **(C)** Individen fungera bra i spelet och ger ifrån sig kommentarer som är väl anpassade utifrån situationen. 
  * **(A)** Individen är välbalanserad och kreativt tillskott

## __Del II:__  Svara på frågor

Några frågor som du ska besvara.

### E nivå:

  1. Skriv en förklaring till hur du har använt klasser i uppgiften ovan.
  2. Förklara en fördel med att anävnda typescript.

### C nivå:

  3. Skriv en förklaring till hur du har använt mix-in i uppgiften ovan.
  4. Förklara hur du skulle ha kunnat använda interface på ett bra sätt. 

### A nivå:

  5. Förklara utifrån koden i denna uppgift vad som menas med polymorfism inom Object orienterad design?
  6. Är det bra att skriva all sin kod i typescript? Motivera ditt svar.

## Kompletering

Om du inte bli klar i tid kommer du få följande kompletering:
*Se också reglerna nedan och behöver du tips så se innehållet ovan*

### För E:  

  * Gör Del I på C nivå.
  * Gör Del II på E nivå.

### För C:

  * Gör Del I på A nivå.
  * Gör Del II på C nivå.

### För A:

  * Fråga om extra uppgift.
  * Gör Del II på A nivå.
  
### Regler för kompletering

Om du inte har lämnat in i tid eller inte är nöjd med ett betyg du fick kan du göra en komplettering. För komplettering gäller:

  * Komplettering kommer innebär att du måste lösa uppgiften på ett delvis annat sätt. Längst ned i inluppen står det hur detta ska göras. *Detta för att undvika fusk och se att rätt kunskaper undersöks.*
  * Du kan inte ställa frågor muntligen, alla frågor måste ske per e-post. E-post kommer besvaras i mån av tid. *Detta eftersom det inte möjligt att komma för läraren att hålla reda på alla detaljerna kring gamla uppgifter när vi börjar med nya.*
  * Vill du ändå ställa en muntlig fråga kan det finns sådan tillfällen som utannonseras via meddelanden på schoolsoft.
  * Om du ställer en fråga se till att du har läst inlämningsuppgiften och de dokument som rekommenderas för att förstå den. Uppge vilken uppgift du har problem med samt vilken eller vilka instruktion du har läst för att klara av uppgiften. 
  * När du är klar med uppgiften, så kommer den rättas i mån av tid. Om din uppgift skicka in för nära inpå betygsättning så kommer den inte hinnas rättas. Se SchoolSoft för när sista dagen är, dagen beror på många olika faktorer.

---

__Copyright © 2018-2019, Magnus Kronnäs__

All rights reserved.
