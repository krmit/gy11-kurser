-- Version 2 2019-02-12
-- Version 1 2018
-- NOTE! You can get this kind of information directly from UHR, their
-- databases are better for serious work.

DROP TABLE IF EXISTS Education;
DROP TABLE IF EXISTS School;
DROP TABLE IF EXISTS Merit;
DROP TABLE IF EXISTS EducationType;
DROP TABLE IF EXISTS AdmissionGroup;
DROP TABLE IF EXISTS Admission;
DROP TABLE IF EXISTS Demand;

-- För utbildningar
CREATE TABLE Education( id           integer primary key,  
					    name         text, -- Namnet på utbildningen.
                        description  text, -- Kort beskrivning på utbildning
                        length       text, -- Längden på utbildning. Använd "year", "week" eller "semesters" för att beskriva hur lång den är.  
                        subject      text, -- Ämne eller branch
                        type_id      integer,  -- Typen av utbildning se EducationType tabellen. 
                        school_id    integer,  -- Vilken skola som utbildningen ges på, se School tabellen
                        study_fee    integer,  -- Hur mycket utbildningen kostar i SEK. Oftast 0 för svenska utbildningar. 
                        note         text      -- Någon extra kommentar.
                        );
                        
-- För olika skolor
CREATE TABLE School(    id           integer primary key,  
					    name         text,		-- Namnet på skolan, unversitet eller företag.
                        description  text,      -- Kort beskrivning av skolan.
                        numer_of_student  integer, -- Ungefär hur många elever som brukar gå där.
                        city         text,      -- Vilken stad  som är huvudort
                        founded_year integer,   -- När skolan grundades.
                        owner        text       -- Vem äger skolan.
                        );

-- Antagninspoäng som behövs för att bli antagen
CREATE TABLE Admission( id           integer primary key,
					    education_id integer,   -- Vilken utbildning som avses
					    year         integer,   -- Vilket år detta värde avser
                        selection    text,      -- Vilken urvals grupp
					    grade        float);    -- Vilken poäng som behövdes i urvalsgruppen för att bli antagen på sista antagnings tillfället.

-- Vilka meriter som behövs för att bli antagen till en utbildning.
CREATE TABLE Demand(    id           integer primary key,
					    education_id text,    -- Vilken utbildning som avses
                        merit_id     integer, -- Vilken merit som avses
                        note         text     -- En notering om detta krav, tex. hur man kan få det. 
                        );
-- En typ av merit som en elev eller student behöver ha, inte att förväxla med sådant som ger meritpoäng.
CREATE TABLE Merit(     id           integer  primary key,
				        name         text,    -- Namnet på meriten, till exempel "Matematik 4".
                        description  text,    -- En beskrivning om det behövas.
                        alternatives text);   -- Eventuella andra meriter som skulle kunna behövas.

-- Vilken typ av utbildning så som universitet program, fristånde kurser, YH etc.
CREATE TABLE EducationType(
                        id           integer primary key,  
						name         text,    -- Namnet på typen av utbildning.
                        description  text     -- En beskrivning av denna typ.
                        );

-- Några bra data att börja med som exempel.

INSERT INTO EducationType VALUES (0, 'Högskola','Utbildning på universitet eller högskola. Kräver grundläggande behörighet ifrån gymnasiet.');
INSERT INTO EducationType VALUES (null, 'Yrkeshögskola','Yrkesutbildning som ska led till jobb efter ett antal veckors studier.');
INSERT INTO EducationType VALUES (null, 'Gymnasieinjengör','Gymnasie utbilkdning som gör till till injengör.');
INSERT INTO EducationType VALUES (null, 'Komvux', 'Kompletera din gymnasieutbildning.');

INSERT INTO School VALUES (0, "Halmstad Högskola", "Mindre högskola", 9608, "Halmstad", 1983, "staten");
INSERT INTO School VALUES (null, "Blekinge Tekniska Högskola", "Mindre högskola", 3200, "Karlskrona", 1989, "staten");
INSERT INTO School VALUES (null, "Chalmers tekniska högskola", "Större högskola", 11000, "Göteborg", 1929, "stiftelse");
INSERT INTO School VALUES (null, "Lunds Universitet", "Större högskola",43000, "Lund", 1666, "staten");
INSERT INTO School VALUES (null, "Kristianstad Högskola", "Mindre högskola", 14000, "Kristianstad", 1977, "staten");

/* Admissions är troligen helt fel! */
INSERT INTO Admission VALUES (0, 0, 2017, "BI", 14.70);
INSERT INTO Admission VALUES (null, 0, 2017, "BF", -1); 
INSERT INTO Admission VALUES (null, 0, 2017, "HP", 0.85);
INSERT INTO Admission VALUES (null, 0, 2017, "BI", 14.70);
INSERT INTO Admission VALUES (null, 1, 2017, "BI", 17.81);
INSERT INTO Admission VALUES (null, 2, 2017, "BI", 14.53);
INSERT INTO Admission VALUES (null, 3, 2017, "BI", 19.32);
INSERT INTO Admission VALUES (null, 4, 2017, "BI", 13.57);
INSERT INTO Admission VALUES (null, 5, 2017, "HP", 1.50);
INSERT INTO Admission VALUES (null, 6, 2017, "BI", 14.33);
INSERT INTO Admission VALUES (null, 7, 2017, "HP", 1.25);
INSERT INTO Admission VALUES (null, 8, 2017, "BI", 17.66);

INSERT INTO Merit VALUES (0, "Fysik 2", "Svår fysik kurs", "Läs på konvux, eller hitta annan utbildning");
INSERT INTO Merit VALUES (null, "Mattematik 3c", "Grundläggande för ingejörs utbildning", "läs Teknik eller natur, eller på konvux");
INSERT INTO Merit VALUES (null, "Mattematik  4", "Bra mattekurs", "Läs på konvux, behövs för civil utbildningar");

INSERT INTO Demand VALUES (0, 5, 0, "Behörighet för civilingejör, Fysik 2");
INSERT INTO Demand VALUES (null, 5, 2, "Behörighet för civilingejör, Matte 4");
INSERT INTO Demand VALUES (null, 10, 1, "Behörighet för högskoleingenjör, matte 3c");
INSERT INTO Demand VALUES (null, 10, 0, "Behörighet för civilingejör, Fysik 2");

INSERT INTO Education VALUES (0, "IT-forensik och informationssäkerhet","Jobba med polisen" , "3 y", "Informationsteknik, IT säkerhet",0,0,0, null);
INSERT INTO Education VALUES (null, "Digital ljudproduktion","Det här är utbildningen för dig som vill arbeta med ljuddesign och ljudberättande i digitala medier." , "3 y", "IT, ljud",0,1,0, null);
INSERT INTO Education VALUES (null, "IT-driftteknikerprogrammet","" , "2 y", "IT-drifttekniker",0,4,0, null);
INSERT INTO Education VALUES (null, "Datasystemutvecklingsprogrammet","" , "3 y", "data utveckling",0,4,0, null);
INSERT INTO Education VALUES (null, "Civilekonom","" , "5 y", "Pengar och tråk",0,0,1, null);
INSERT INTO Education VALUES (null, "Civilingenjör i maskinteknik","" , "5 y", "Maskin och trams",0,1,2, null);
INSERT INTO Education VALUES (null, "Maskinteknik, Civilingenjör","" , "5 y", "Maskin och sånt",0,2,3, null);
INSERT INTO Education VALUES (null, "Nätverksteknik","" , "3 y", "it säkerhet",0,1,4, null);
INSERT INTO Education VALUES (null, "Datateknik, civilingenjör","" , "5 y", "Lär dig om datorteknik",0,2,5, null);
INSERT INTO Education VALUES (null, "Civilingenjör i maskinteknik","" , "5 y", "Maskiner mm",0,1,6, null);
INSERT INTO Education VALUES (null, "Högskoleingenjörsutbildning i datateknik","" , "3 y", "datateknik",0,3,7, null);
INSERT INTO Education VALUES (null, "Datateknik, högskoleingenjör","" , "3 y", "datateknik",0,2,8, null);
