# Innehåll
## Skap en egen databas över något intressant.
![Databas](../../image/database.png)

Om olika lösningar fördatabase vid webbserverprogrammering. Framför allt SQL baserade databaser.

## Översikt

  * [Inlämning](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel 
  * Databasmodeller
  * SQL och Implementeringar av relationsdatabaser
  * NoSQL


## Guider

  * [Installation av Mint som ett ensamt OS på en dator](http://htsit.se/a/topics/computer/os/linux/Installation-of-linux/Tutorials/installation-of-Mint-as-a-single-OS.html)
  * [Installation av Mint som en av många OS]( http://htsit.se/a/topics/computer/os/linux/Installation-of-linux/Tutorials/installation-of-Mint-as-one-of-many-OS.html)

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
