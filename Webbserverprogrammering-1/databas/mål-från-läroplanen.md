# Mål från läroplanen
## Om mål för att hantera användares information

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Datalagring i relationsdatabas eller med annan teknik.

## Kunskapskrav

### E                   

  * Inga

### C                   

 * Applikationen ... tillhandahåller en enkel lösning för permanent datalagring.

### A                   

 * Applikationen … tillhandahåller en lösning med viss komplexitet för permanent datalagring.
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

