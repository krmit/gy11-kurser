# Innehåll
## Skap en egen databas över något intressant.
![Databas](../../image/database.png)

Du ska skapa en egen databas och söka information i en något större databas.

## Regler

  * När du är klar ska du packa ihop alla filer med zip till en fil.
  * Gå till följande sida och ladda upp din zip fil.
  * På första lektionstillfället efter inlämningsdagen kan du få svara på frågor om uppgiften.
  * Blir du inte godkänd på denna inlämning kommer du få göra en komplettering enligt beskrivningen nedan.

Du får själv välja namnen på filerna, men de ska vara beskrivande av vad de innehåller så det är lätt att se vilken del eller uppgift de tillhör.

**OBS!**
Du måste lämna in i tid, se till att skicka in allt du har gjort senast vid deadline. Om du inte har gjort det så kommer du få en instruktion för att komplettera i SchoolSoft.

## Bedömning

Din bedömning kommer bli den lägsta nivån som du får på de olika delarna nedan. Om du klarat en del på en högre nivå än de andra delen kommer ditt betyg bli det lägsta mellanbetyget som är högre än ditt lägsta resultat. Med andra ord om du får E och A, så blir ditt resultat D.

Observera att en bedömning på denna uppgift som visar att ditt resultat är F är också en F-varning. Du kommer då behöva göra komplettering för E nivå.


## Del I: Skapa en egen databas

Vi tänker oss att vi har ett lite finansiell bolag i något skatteparadis utan en fungerande finansinspektion. Vi  behöver en databas över våra kunder tillgångar.

Du kan förslagsvis använda sqlite till att skapa denna databas.
**sqlite3 bank.db**

Redovisa all sql kod du skriver för att lösa uppgiften och själva databasen som blir resultatet. Du kan spara ned din databas genom att dumpa den till sql kod i en separat fil. I sqlite är det kommandot ".dump" som ger tabellen som sql kod. Du kan sedan spara detta i en fil med filändelsen ".sql".

Se också till att du bifogar en README fil där du mycker kortfattat förklarar hur du har tänkt och ger kommentarer till uppgiften du tycker något är oklart.


**Allt i din databas ska vara bra som möjligt utifrån de principer vi har diskuterat.**

### Nivå E:

  * Skapa en tabell “Customer”. Denna tabell ska förutom id, ha attribute för:
    * Namn på kunden.
    * kön
    * ålder
    * telefonnumer
    * address
    Du kan själv välja namnet och datatypen på dessa kolumner. I sqlite behöver du inte ange datatyp, men det kan vara bra ändå.
  * Lägg till minst fyra olika kunder till denna “Customer” tabell.
  * Uppdatera ett värde för en av dessa kunder.
  * Ta bort ett valfritt rad från en av tabellerna.

### Nivå C:

  * Skapa en tabell “capital”, detta konto representerar hur mycket pengar som kunder har satt in i banken. Normal borde det värde vara negativt så länge kunden har pengar på banken. Välj själv lämpliga attribute. 
  * Skapa en tabell som representerar hur mycket pengar en kund har placerat i ett konto. Det borde innehålla hur många andelar som en kund äger och värdet på dessa.
  * Lägg till test data till båda tabellerna så de har minst fyra värden.

### Nivå A:

Du får designa om din databas för att uppnå kravet nedan. Men du kan också välja att utöka den databas du redan har. Om du gör om det som du gjort på nivå E och C, så redovisa dessa olika databaser var för sig.

  * Lägg till ytterligare minst en typ av konto. 
  * Gör en tabell “transaction” som ska representera en överföring mellan konton.
  * Inför test värden så alla din tabeller har minst fyra värden.
  * Gör ett sql kommit som motsvara en överföring av pengar mellan två konton och som även uppdaterar tabellen transaktion. Det räcker med att du skriver alla kommandon som du tänker dig är en transaktion.

## Del II: Gör sökningar i en databas.

Du ska utgå ifrån en databas om antagning, som nedan finns i några olika format. 
Sql kod [antagning.sql]https://htsit.se/f/antagning.sql) 
Databas till sqlite: [antagning.db](https://htsit.se/f/antagning.db) 

Redovisa svaret och sql koden som du behöver för att finna svaret i uppgifterna nedan om inget annat anges.

### Nivå E:

  * Hur många utbildningar finns det i databasen? (SQL kod behövs inte)
  * Visa rad för utbildningen som har namnet: “”
  * Visa alla utbildninga som börjar med “Civilingenjörsutbildning”

### Nivå C:
Förfrågan nedan ska du redovisa sql kod och ge en utförlig beskriva vad koden gör.

  * Räkna utbildningen som börjar med “Civilingenjörsutbildning”, din sql kod ska visa antalet i siffror.
  * Visa namnet på alla utbildningar som har mer än 200 sökande.
  * Byt namnet på kolumnerna “name_of_education” till “namn” och “total_searching” till “ansokningar”. Observera att detta inte är beskrivet i någon länk som givits utan kommandot kan behöva googlas fram.

### Nivå A:

  * Gör en sökning som visar vilken utbildning som är populärast.
  * Ta bort alla kolumner förutom “namn” och  “ansokningar”. Gör detta med hjälp av SQL kod. Observera att detta inte är beskrivet i någon länk som givits utan kommandot kan behöva googlas fram.

---

__Copyright © 2018-2020, Magnus Kronnäs__

All rights reserved.

