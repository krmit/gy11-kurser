# Innehåll
## Hantera en server och skydda den
![Säkerhets bugg](../../image/security-bug.png)

Olika sätt att hantera och skydda en server.

## Översikt

  * [Prov](http://htsit.se/e/webbprog1-hacking)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * Underhåll av en webbserver
  * Hantera ett domännamn
  * Moderna verktyg såsom Docker och Ansible
  * Hacking och datasäkerhet

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
