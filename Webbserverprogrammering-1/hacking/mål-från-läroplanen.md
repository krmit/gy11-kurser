# Mål från läroplanen
## De som ska läras om gråzonerna

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Kvalitetssäkring av dynamiska webbapplikationers funktionalitet, säkerhet ....
  * Grundläggande säkerhet och sätt att identifiera hot och sårbarheter samt hur attacker kan motverkas genom effektiva åtgärder.


## Kunskapskrav

### E                   

 * Eleven identifierar ett fåtal sårbarheter eller hot och vidtar enkla åtgärder för att förhindra att produkten utnyttjas. 
 * När eleven kommunicerar med andra använder hon eller han med viss säkerhet enkel terminologi.

### C                   

 * Eleven identifierar sårbarheter eller hot och vidtar åtgärder för att förhindra att produkten utnyttjas. 
 * När eleven kommunicerar med andra använder hon eller han med viss säkerhet terminologi.

### A                   

 * Eleven identifierar flera sårbarheter eller hot och vidtar avancerade åtgärder för att förhindra att produkten utnyttjas. 
 * När eleven kommunicerar med andra använder hon eller han med säkerhet terminologi.

 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

