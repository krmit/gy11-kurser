# Webbserverprogrammering 1

![Min server](../image/server.png)

Webbserverprogrammering 1 handlar om hur du skapar en bra dynamisk sajt. I denna kurs ska lära dig att skapa en webbapplikation. Med hjälp av nodejs och andra verktyg ska vi lära oss att skapa en webbapplikation.

## Moment

- [Introduktion till webb](./webb/index.html)
- [Introduktion till nodejs](./node/index.html)
- [Hantera och lagra information ifrån användare](./databas/index.html)
- [Hantera en server](./hacking/index.html)
- [Skapa en sajt](./skapa-en-sajt/index.html)

[**Kursplanen**](../plans/webbserverprogrammering.pdf)

_Tillbaka till [Kurslista](../kurslista.html)_

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
