# Innehåll
## Lära dig om node
![Node med js](../../image/node-js.png)

Att programmera node

## Översikt

  * [Inlämning](./submission.sv.html)
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel

  * [Introduktion till nodejs](https://htsit.se/a/topics/programming/js/node/Introduction-to-node/index.html)
  * Att använda npm
  * Vi dokumenterar vårt program

## Guider

  * [Installation av node js på Linux](https://docs.google.com/document/d/1pwf5_ExAR8vbebDv0f53T0h8Lg1sja5wLsDtdjGC3HE/edit?usp=sharing)
  * Ej klart eller utan support. [Installation av node js på Windows](https://docs.google.com/document/d/1jXf597GCLtWVljeGEH_-tl1pAjgJLrl9L-cr9-y2FPU/edit?usp=sharing)
  * [Installation av senaste node på linux](http://htsit.se/a/topics/programming/js/node/Introduction-to-node/Tutorials/installation-of-current-node.html)
  * [Att starta ett node projekt](https://docs.google.com/document/d/1BrfVvkDjOY7h9tjWRzBNRObiLQ8MW906ymvZQosPQMQ/edit?usp=sharing)
	
_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

__ All rights reserved. __
