# Ett enkelt terminal program med node

En introduktion till att programmera med node. Ett enklare program ska 
göras med hjälp av packethanteraren.

## Innehåll

Läs igenom [lektionesplaneringen](http://bit.ly/htsit-w-2) och [kursmaterialet](https://htsit.se/k/Webbserverprogrammering-1/node/index.html).

## Regler

  * Du ska skicka svaret som en e-post till “magnus.kronnas@hassleholm.se”.
  * Ämnet på brevet ska vara “Introduktion till nodejs””.
  * Du ska också skriva ditt namn i brevet.
  * Är ämnet fel eller brevet skicka in för sent, dvs. inte dagen innan redovisning av uppgiften, kommer uppgiften inte att bedömas.
  * På redovisningsdagen kommer du få svara på frågor om din inlämning.
  * Du får inte visa din kod för någon annan förän efter redovisningen för alla.
  * Se SchoolSoft för deadline som normalt är klockan 22:00 dagen innan redovisningsdagen.
  * Du får inte visa din kod för någon annan förrän efter redovisningen för alla. Du får mycket gärna svara på frågor från andra elever, men du får inte hjälpa dem skriva kod.
  * Om samma icke triviala kod skulle lämnas in av fler än en elev, kommer dessa elever ha gjort sig skyldiga till fusk.
  * Om du kopierar en lösning på ett problem från er webb källa, t.ex. stackoverflow, så ska du i en kommentar till koden ha en länk till originalet. Avvikelser mot detta kan vara fusk beroende på omfattningen och hur koden har används.

  
**Obs!** Det är viktigt att du lämnar in i tid. Om du inte lämnar in i tid kommer det på SchoolSoft skickas ett meddelande med instruktioner för sen inlämning. Dessa kan inkluder kompletterings uppgifter som skiljer från dem som du får här. Det kommer antas att du fått F på detta moment fram till inlämning har gjorts och du därför också är F varnad.

## Bedömning

Din bedömning kommer bli det lägsta nivå som du klara alla steg för nedan. Om du några steg på en högre nivå kan du få ett mellanbetyg. Så om du har klara alla steg på E nivå och flera väsentliga steg på A nivå så får du D i betyg.

Om ett steg har flera nivåer, till exempel “(C/A))” så är nivån beroende på vilken nivå du har på det steg som du redan löst.


Observera att en bedömning på denna uppgift som visar att ditt resultat är F är också en F-varning. Du kommer då behöva lämna in uppgiften igen för att kunna bli godkänd på kursen. Du får instruktioner för detta via SchoolSoft.

Om du är missnöjd med betyg så kan du också lämna in uppgiften igen. Observera dock att du först ska skicka ett mail till mig så att du kan får instruktioner för hur du ska komplettera. För visa betyg kan också uppgiften förändras när du vill komplettera.

All diskussion om bedömningar av uppgiften efter redovisningsdagen ska ske via e-post, läraren ger inte muntliga svar på annat sätt förutom på resurstiden på fredag mellan 14.30 och 15.30. Observera att en sådan diskussion måste ske minst en vecka innan betygen sätts.

## Beskrivning

Du ska göra ett enkelt räknespel. Det ska ställa olika räknefrågor till en användare såsom “Vad är 1+2?” och se om användaren kan svara på dessa.

### Steg 1: Börja med ett node projekt
  1. Skapa en mapp för spelet. (E)
  2. Kopiera in en packet.json fil och anpassa den till projektet. (E)
  3. Kopiera in en index.js fil som som vi har haft i exemplena, få projektet att köras med inde.js filen och skriva ut en enkel text. (E)
  4. Skriv en README.md fil med ditt namn i filen. (C)
  5. Se till att projektet mappen är ett git arkiv. (C)
  
### Steg 2: Skapa ett eget räknespel

  1. Se till att programmet ställer en fråga om att addera två tal. (E)
  2. Efter frågorna har ställts presentera ett resultat. (E)
  3. Gör frågor för flera olika räkneopratorer. (C)
  4. Vilken operator som ska användas ska bestämmas med flaggor till programmet.(A)

### Steg 3: Spara resultat

  1. Spara resultaterna i en fil. (E)
  2. Se till att formatet är yaml, eller ett liknande lättläst dataformat förutom json. (C)
  3. Visa i programmet statestik baserat på de data sim du har sparat. (A)

### Steg 4: Gör förbättringar

  1. Se till att frågorna är varierande och det finns flera olika typer av frågor.(C)
  2. Var kreativ i hur du skapar nya frågor och ditt upplägg av programmet. Du kan till exempel använda fler paket och komma på nya sätt att ställa frågor. (A)
  3. Gör så att du kan välja olika typer av frågor beroende på flagor som programmet får. (A)

### Steg 5: Avsluta

  1. Updatera README.md så det beskriver programmet som du har skapat. (C/A)
  2. Paketera med zip och ge zip filen ditt alias som namn. (E)
  3. Skicka in det till lärarens e-postadress med ämnet “Introduktion till nodejs” (E)

**Lycka till!**

**/krm**


---

__Copyright © 2018-2019, Magnus Kronnäs__

__ All rights reserved.__
