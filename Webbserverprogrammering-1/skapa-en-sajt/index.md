# Innehåll
## Hur du skapar din egen sajt
![Att bygga sin egen sajt](../../image/build-website.png)

Vi använder oss av hjälpprogram easy-web-framwork.

## Översikt

  * [Inlämning](http://htsit.se/e/webbprog1-5-sajt), [sammanfattning.](./submission.sv.html).
  * [Mål ifrån läroplanen](./mål-från-läroplanen.html)

## Kapitel och ämnen

  * Statiska webbsidor
  * Hantera anrop med en webbserver
  * Template med Mustasch
  * Använda cookies och sessionsvariabler
  * Använda en databas i en sajt
  * Klientsidan av en webbsajt
  * _Introduktion till React.js_

## Guider

  * _Skapa en egen sajt_

_ Tillbaka till [översikt](../index.html) _

---
__ Copyright © 2018-2019, Magnus Kronnäs __

All rights reserved.
