# Mål från läroplanen
## Om hur eleven sajt ska vara

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Dokumentation av utvecklingsprocess och färdig produkt.
  * Applikationsarkitektur och separation av olika slags logik.
  * Kvalitetssäkring av dynamiska webbapplikationers ... kodkvalit.


## Kunskapskrav

### E                   

 * Eleven gör en enkel projektplan för en tänkt webbapplikation. 
 * I projektplanen beskriver eleven översiktligt applikationens funktion.
 * Utifrån projektplanen utvecklar eleven i samråd med handledare applikationen där presentations­logiken i begränsad utsträckning är skild från applikationens övriga logik.
 * Applikationen uppvisar enkel funktionalitet med ett språk för webbserver­programmering och tillhandahåller en enkel lösning för datapersistens mellan sidvisningar.
 * Produkten är av tillfredsställande kvalitet och följer etablerad god praxis.
 * När arbetet är utfört gör eleven en enkel dokumentation av de moment som har utförts samt utvärderar med enkla omdömen sitt arbete och resultat. 

### C                   

 * Eleven gör en genomarbetad projektplan för en tänkt webbapplikation.
 * I projektplanen beskriver eleven utförligt applikationens funktion och arkitektur.
 * Utifrån projektplanen utvecklar eleven efter samråd med handledare applikationen där presentations­logiken är skild från applikationens övriga logik.
 * Applikationen uppvisar funktionalitet med ett språk för webbserver­programmering … .
 * Produkten är av tillfredsställande kvalitet och följer etablerad god praxis. Detta kontrollerar eleven med hjälp av några tester.
 * När arbetet är utfört gör eleven en noggrann dokumentation av de moment som har utförts samt utvärderar med nyanserade omdömen sitt arbete och resultat. 

### A                   

 * Eleven gör en genomarbetad projektplan för en tänkt webbapplikation. Vid behov reviderar eleven planen. 
 * I projektplanen beskriver eleven utförligt och nyanserat applikationens funktion, arkitektur och mjukvarugränssnitt.
 * Utifrån projektplanen utvecklar eleven efter samråd med handledare applikationen där presentations­logiken är skild från applikationens övriga logik.
 * Applikationen uppvisar funktionalitet med ett språk för webbserver­programmering för att driva en helt databaserad webbplats eller en jämförbart avancerad produkt … .
 * Produkten är av god kvalitet och följer etablerad god praxis. Detta kontrollerar eleven med hjälp av flera tester, även manuella.
 * När arbetet är utfört gör eleven en noggrann och utförlig dokumentation av de moment som har utförts samt utvärderar med nyanserade omdömen sitt arbete och resultat samt ger förslag på hur arbetet kan förbättras. 
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

