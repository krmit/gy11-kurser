# Inför prov om webben

## Lära dig om webben

![Node med js](../../image/node-js.png)


## Innehåll

Gå igenom kapitlet nedan, se till att du förstår innehållet. Observera att det ofta bara minnesanteckningar och inte komplett genomgångar av ämnet. Är du något som du har glömt bort från de muntliga genomgångar eller inte förstår skicka ett mail till mig. I mailet precisera vad det är du inte förstår och på vilket sätt du har försökt ta reda på detta.

- [Introduktion till Webben](http://www.htsit.se/a/topics/web/Introduction-to-web/index.html)
  - [En övrsikt över webben](http://www.htsit.se/a/topics/web/Introduction-to-web/00-Overview/index.html)
  - [Server-klient kommunikation](http://www.htsit.se/a/topics/web/Introduction-to-web/02-Server-client/index.html)
  - [URL](http://www.htsit.se/a/topics/web/Introduction-to-web/03-Url/index.html)
  - [Webbklient](http://www.htsit.se/a/topics/web/Introduction-to-web/04-Web-client/index.html)
  - [Webbläsare](http://www.htsit.se/a/topics/web/Introduction-to-web/05-Web-browser/index.html)
  - [Webbservrar](http://www.htsit.se/a/topics/web/Introduction-to-web/06-Web-server/index.html)
  - [Dataformat](http://www.htsit.se/a/topics/web/Introduction-to-web/07-Data-format/index.html)
  - [Protokol](http://www.htsit.se/a/topics/web/Introduction-to-web/09-Protocol/index.html)
  - [HTTP](http://www.htsit.se/a/topics/web/Introduction-to-web/10-Http/index.html)
  - [TLS, säkerhet på internet.](http://www.htsit.se/a/topics/web/Introduction-to-web/11-Tls/index.html)
  - [RestFull](http://www.htsit.se/a/topics/web/Introduction-to-web/12-RestFull/index.html)
  - [WebSocket](http://www.htsit.se/a/topics/web/Introduction-to-web/13-Websocket/index.html)
  - [Teckenkodningar](http://www.htsit.se/a/topics/web/Introduction-to-web/14-Character-encoding/index.html)

Du kan också gå igenom följande övningar som du hittar på [mimer examanitor](https://htsit.se/e):
  * __Introduktion till webben__
  * __Om protokol och format__
  * __Webbservara och webbklienter__
  * __Om protokol och format__
  * __Om teckenkodning__

Dessa sammanfattas i den stora frågeövningen:

  * __Alla frågor__

Frågorna som kommer i den första delen av provet bygger på dessa frågor och de kommer också var till hjälp för alla delar.

## Bedömning:

Bedömningen beror på totalpoäng, men också på hur väl du klarar de reflekterande frågorna i del II och del III samt om du visat på särskilt goda kunskaper under lektionerna. På ett ungefär kan följande tabell användas men avvikelser i bedömning kan förekomma.

  * __E: 10__
  * __D: 13__ (Var av minst 1 poäng på Del II) 
  * __C: 16__ (Var av minst 2 poäng på Del II) 
  * __B: 19__ (Var av minst 1 poäng på Del III) 
  * __A: 21__ (Var av minst 2 poäng på Del III) 

__Max: 24__

_Tillbaka till [introduktion](./introduktion.html)_

---

**Copyright © 2018, Magnus Kronnäs**

All rights reserved.
