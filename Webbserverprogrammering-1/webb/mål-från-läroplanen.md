# Mål från läroplanen
## Vilka grunder som behöver kunnas

## Kommentarer om läroplannen 



_Tillbaka till [introduktion](./introduktion.html)_

## Centralt innehåll

  * Webbserverns och dynamiska webbplatsens funktionalitet.
  * Utvecklingsprocessen för ett webbtekniskt projekt med målsättningar, krav, begränsningar, planering och uppföljning.
  * En översikt över olika lösningar eller språk som finns för att skapa dynamiska webbplatser.
  * Teknisk orientering om webbens protokoll, adresser, säkerhet samt samspelet mellan klient och server.
  * Teckenkodning. Begrepp, standarder och handhavande.
  * Specifikation av struktur och design, kodning, testning samt driftsättning.
  * Dokumentation av utvecklingsprocess och färdig produkt.
  * Grundfunktionen i ett programspråk för dynamiska webbplatser.
  * Kodning och dokumentation enligt vedertagen praxis för vald teknik.


## Kunskapskrav

### E                   

 * Eleven beskriver översiktligt tekniken bakom dynamiska webbplatser och hur de samspelar med olika tekniker på webben.
 * I arbetet utvecklar eleven kod som i begränsad utsträckning är läsbar och översiktligt kommenterad. 

### C                   

 * Eleven beskriver utförligt tekniken bakom dynamiska webbplatser och hur de samspelar med olika tekniker på webben.
 * I arbetet utvecklar eleven kod som följer en given kodningsstandard och är översiktligt kommenterad.

### A                   

 * Eleven beskriver utförligt och nyanserat tekniken bakom dynamiska webbplatser och hur de samspelar med olika tekniker på webben.
 * I arbetet utvecklar eleven kod som följer en given kodningsstandard och är utförligt kommenterad. 
 
---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.

