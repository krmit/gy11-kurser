# Innehåll
## Allt om http
![http](../../image/html.png)

Här ska se om du lärt dig grunderna i html.

## Innehåll

Läs igenom [lektionesplaneringen](http://bit.ly/htsit-wu-2) och [kursmaterialet](./index.html).

## Regler

  * Du ska skicka svaret som en e-post till “magnus.kronnas@hassleholm.se”.
  * Ämnet på brevet ska vara “Introduktion till html””.
  * Du ska också skriva ditt namn i brevet.
  * Är ämnet fel eller brevet skicka in för sent, dvs. inte dagen innan redovisning av uppgiften, kommer uppgiften inte att bedömas.
  * På redovisningsdagen kommer du få svara på frågor om din inlämning.
  * Du får inte visa din kod för någon annan förän efter redovisningen för alla.
  * Se SchoolSoft för deadline som normalt är klockan 22:00 dagen innan redovisningsdagen.
  * Du får inte visa din kod för någon annan förrän efter redovisningen för alla. Du får mycket gärna svara på frågor från andra elever, men du får inte hjälpa dem skriva kod.
  * Om samma icke triviala kod skulle lämnas in av fler än en elev, kommer dessa elever ha gjort sig skyldiga till fusk. Det gäller även om du skrev koden för du har brytit mot punkten ovan.
  
**Obs!** Det är viktigt att du lämnar in i tid. Om du inte lämnar in i tid kommer det på SchoolSoft skickas ett meddelande med instruktioner för sen inlämning. Dessa kan inkluder kompletterings uppgifter som skiljer från dem som du får här. Det kommer antas att du fått F på detta moment fram till inlämning har gjorts och du därför också är F varnad.

## Bedömning

Din bedömning kommer bli det lägsta nivå som du klara alla steg för nedan. Om du några steg på en högre nivå kan du få ett mellanbetyg. Så om du har klara alla steg på E nivå och flera väsentliga steg på A nivå så får du D i betyg.

Om ett steg har flera nivåer, till exempel “(C/A))” så är nivån beroende på vilken nivå du har på det steg som du redan löst.

Observera att en bedömning på denna uppgift som visar att ditt resultat är F är också en F-varning. Du kommer då behöva lämna in uppgiften igen för att kunna bli godkänd på kursen. Du får instruktioner för detta via SchoolSoft.

Om du är missnöjd med betyg så kan du också lämna in uppgiften igen. Observera dock att du först ska skicka ett mail till mig så att du kan får instruktioner för hur du ska komplettera. För visa betyg kan också uppgiften förändras när du vill komplettera.

All diskussion om bedömningar av uppgiften efter redovisningsdagen ska ske via e-post, läraren ger inte muntliga svar på annat sätt förutom på resurstiden på fredag mellan 14.30 och 15.30. Observera att en sådan diskussion måste ske minst en vecka innan betygen sätts.

## Beskrivning

Du ska göra en enkel html sida. Du ska själv välja vilket ämne sidan ska handla om. Du får inte välja ett ämne som är samma eller liknande någon annans elevs, säg därför vilket ämne du har valt muntligen till läraren.

### Steg 1: Börja med en html sida
  1. Skapa ett html document, namnet ska vara ditt alias. (E)
  2. Kopiera in innehållet från [mall.html](https://htsit.se/f/mall.html). (E)
 
Behöver du mer tips på hur du kan komma igång med utvecklingen se [denna](https://docs.google.com/document/d/17O65J4x4KsfQrWqAfYJFN5gdKMrN9m1nt-Bd0lTsMCA/edit?usp=sharing) guide.

### Steg 2: Se till att den har bra struktur
  1. Gör så du har en titel på sidan och ett stycke med text. (E)
  2. Gör så att du har en lista med några punkter på din sajt. (E)
  3. Skapa minst tre olika länkar på sidan.  (E)
  4. Lägg till minst tre element för rubrik, paragraf eller lista på sidan. (C)
  5. Ha minst två bilder i html sidan. Dessa bilder måste bifogas till mailet som du skickar in och ska då ha rätt namn.(C)
  6. Se till att använda olika element i texten för att markera specifika delar i texten. (C)
  7. Använd div för att skapa mer struktur på sidan. (A)
  8. Inför klasser och id för att det lätt ska gå att “välja” olika delar av sidan. (A) 
  9. Sidans struktur ska vara lätt att förstå och vara tydlig. (A)
10. Sidan ska på ett bra sätt representera den information om ämnet du har. (A)

### Steg 3: Se att style hjälper till att se strukturen
  1. Använd style attribute för att ändra minst fyra olika CSS egenskaper. (C)
  2. Se till att genom att sätta style attribute får sidan ett utseende som framhåller dess struktur. (A)

Observera att du inte ska göra en separat CSS fil.

### Steg 4: Avsluta
  1. Titta igenom sidan och se att det inte finns uppenbara fel med html koden. (E)
  2. Se till att sidan valideras enligt W3Schools validerare. (C)
  3. Skicka din html sidan inklusive bifogade filer enligt ovan till “magnus.kronnas@hassleholm.se”, ämnet ska vara “Introduktion till html”. (E)


**Lycka till!**

**/krm**


---

__Copyright © 2018-2019, Magnus Kronnäs__

All rights reserved.
