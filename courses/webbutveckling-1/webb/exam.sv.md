# Faktaprov om webben

## Grunderna i webb

![Node med js](../../image/web-history.png)

## Innehåll

Gå igenom kapitlet nedan, se till att du förstår innehållet. Observera att det ofta bara minnesanteckningar och inte komplett genomgångar av ämnet. Är du något som du har glömt bort från de muntliga genomgångar eller inte förstår skicka ett mail till mig. I mailet precisera vad det är du inte förstår och på vilket sätt du har försökt ta reda på detta.

- [Introduktion till Webben](http://www.htsit.se/a/topics/web/Introduction-to-web/index.html)
  - [En övrsikt över webben](http://www.htsit.se/a/topics/web/Introduction-to-web/00-Overview/index.html)
  - [URL](http://www.htsit.se/a/topics/web/Introduction-to-web/03-Url/index.html)
  - [Webbklient](http://www.htsit.se/a/topics/web/Introduction-to-web/04-Web-client/index.html)
  - [Webbläsare](http://www.htsit.se/a/topics/web/Introduction-to-web/05-Web-browser/index.html)
  - [Dataformat](http://www.htsit.se/a/topics/web/Introduction-to-web/07-Data-format/index.html)
  - [Protokol](http://www.htsit.se/a/topics/web/Introduction-to-web/09-Protocol/index.html)

Du kan också gå igenom följande övningar som du hittar på [mimer examanitor](https://htsit.se/e/dashboard.html#/t2):

  * __Allt om webben för webbdesignare - Del 1__
  * __Allt om webben för webbdesignare - Del 2__

Dessa sammanfattas i den stora frågeövningen: Allt om webben för __webbdesignare__


## Regler

  * Inga hjälpmedel 
  * Provet är uppdelat i tre delar.
    * Del I består av 7 frågor som ger 2 poäng var.
    * Del II består av 2 frågor som ger 3 poäng var.
    * Del III består av 1 frågor som ger 4 poäng.
 * Följande kan ge poängavdrag:
    * Använt fel terminologi
    * Skrivit irrelevanta eller felaktiga påståenden.
 * I Del I så ska svaret vara en fullständig mening eller beräkning om det inte står att du kan besvara frågan på annat sätt.
 * Varje uppgift på Del II och Del III kan innehålla ytterligare regler för hur du får poäng, läs uppgift instruktionen noggrant.

## Bedömning:

Bedömningen beror på totalpoäng, men också på hur väl du klarar de reflekterande frågorna i del II och del III samt om du visat på särskilt goda kunskaper under lektionerna. På ett ungefär kan följande tabell användas men avvikelser i bedömning kan förekomma.

  * __E: 10__
  * __D: 12__ (Var av minst 1 poäng på Del II eller Del III) 
  * __C: 14__ (Var av minst 2 poäng på Del II eller Del III) 
  * __B: 17__ (Var av minst 1 poäng på Del III) 
  * __A: 19__ (Var av minst 2 poäng på Del III) 

__Max: 24__

_Tillbaka till [introduktion](./introduktion.html)_

---

**Copyright © 2018, Magnus Kronnäs**

All rights reserved.
