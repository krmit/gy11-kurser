# Förklaringar till kursuppläggen

## Från mimer-academy

Är arkivet där jag spara alla konkret materia för kurserna. Detta projekt följer alltid en pedagogiska idé och tar inte hänsyn till ämnesplanerna och andra styrdokument. Utan det är detta dokument _gy11-kurser_ som förklara hur _mimer-academy_ ska användas i undervisning.
 
  * __kapitel__ Ett kapitel är en tydlig pedagogisk model för att lära ut något.
  * __ämnen__ Varje kapitel är sorterade i ämnen
  * __avsnitt__ Varje kapitel äv uppdelat i avsnitt.
  
## Från gy11-kurser 
  * __läroplaner__ En ämnesplan är uppdelat i flera kursplaner. Dessa planer inklusive examensmål ligger till grund för all undervisning och kallas läroplan.
  * __kurs__ Är den praktiska enhet i vilken all undervisning bedrivs i i enlighet med läroplanen.
  * __moment__ Varje ämnesplan är uppdelat i ett antal moment, normalt fyra stycken. Till varje moment finns det ett urval av centralt innehåll och kunskapskrav som vi fokuserar på. Varje moment avslutas med antingen en inlämningsuppgift eller ett prov. Alla moment har också ett antal test som ges kontinuerligt med eleverna arbetar med momentet.

### Från moment
  * __introduktion__ Innehåller en introduktion till kursen.
  * __innehåll__ Innehåller en lista på alla kapitel, guider och uppgifter som ska gås igenom under kapitlet.
  * __test__ Basrade på frågor ifrån kapitlerna. Dessa test kan antingen klaras av när det ges kontinuerligt under kursens gång. Eller som ett omtest vid slutet av ett moment.
  * __inlämning__ En större inlämningsuppgift som ges vid slutet av ett moment. Varje inlämningsuppgift är indelat i flera steg. Dessa steg kategoriseras utifrån en E, C eller A nivå.
  * __prov__ Ett prov som ges vid slutet av kursen och består av uppgifter. Betygen sätts utifrån hur många total påong eleven får på provet. Uppgifterna är dock valda så att för att få de högsta totalpoängen måste du klara uppgifter som motsvara kunskarav för A för aktuellt moment. 
  
### Från innehåll, guider, inlämningar och prov.
  * _en kursivt text_ betyder att innehållet är extra och kan göras om eleven vill men annars kan hoppas över. 

---
__Copyright © 2018, Magnus Kronnäs__

All rights reserved.
