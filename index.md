# Kursupplägg för IT kurser i gy11 av krm

![Logo for HTSIT](./image/htsit.png)

För att förstå strukturen och innehållet i dessa kurser läs [denna](./förklaringar.html) förklaring.

## Kurser

- [Gemmensamma](./Common/index.html)
- [Programmering 1](./Programmering-1/index.html)
- [Programmering 2](./Programmering-2/index.html)
- [Webbserverprogrammering 1](./Webbserverprogrammering-1/index.html)
- [Dator- och nätverksteknik](./Datorteknik/index.html)
- [Elektronik och mikrodatorteknik](./Elektronik/index.html)
- [Webbutveckling 1](./Webbutveckling-1/index.html)

---

**Copyright © 2018-2019, Magnus Kronnäs**

All rights reserved.
