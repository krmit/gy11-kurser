#!/bin/bash

# Uppgiftslista
# Namn: {{id}}
# Inlämning: {{deadline}}

#  * Skriv en kommentar till varje kommando!
#  * Skriptet ska vara körbart.

# Du ska skriva ett enkelt program som loggar små medealnden.
# Ditt script ska ta en inpameter som ska vara ett medelande som ska 
# loggas. Tips: $1 kommer vara denna parameter i ditt script.
#  
# 1. Låt ditt skript skriva ut "Do logs for {{name}}". 
# 2. Lägg till dagens datum till filen "log.txt"
# 3. Lägg till medelandet till logg filen och en ny rad.

# Lycka till!

# Din script kod nedanför här
