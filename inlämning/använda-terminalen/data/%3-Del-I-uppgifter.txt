# Uppgiftslista
Namn: {{id}}
Inlämning: {{deadline}}

Glöm inte bort att du måste skriva ned varje kommando som du gör under
respektive uppgift nedan!

Se också reglerna i README

Lycka till!

1. Gå till mappen _Computer_.

2. Gå till mappen _Computer/People/Linus_.

3. Lista alla filer som finns i mappen.

4. Välj en av bilderna och byt namn till _min-favorit.jpg_ .

5. Ta en annan bild och kopiera den till mappen _Computer/People/Jeri_.

6. Gå till mappen “Jeri”.

7. Du har ångrat dig så ta bort bilden som du kopierade hit.

8. Gör ett kommando som tar dig till _Computer/Game/Undertale_

9. Gå tillbaka till den mapp som denna uppgiftslistan ligger i.

10. Hitta filen som heter "Linus.jpg" som finns i mappen "Animals", med hjälp av ett bash kommandon.

11. Gå till den mappen, som filen ovan ligger i.

12. Byt namn på bilden till något långt och trevligt.

13. Gå till mappen Computer/People/RMS

14. Läs filen _gpl-3.0.txt_. Du behöver använda ett terminalkommando för att se innehållet.

15. Gå till mappen "Animals".

16. Skapa en mapp med namnet "{{id}}-min-hörna".

17. Gå in i denna mapp.

18. Ladda ned en valfri bild till denna mapp. Återigen ska du göra detta med ett terminalkommando.

19. Skapa en textfil med valfritt namn.

20. Öppna den men en terminal editor och skriv en valfri mening som innehåller orden/ordet "{{description}}" och spara denna mening. Skriv både kommandot du använd för att editera filen och meningen du skrev undern denna uppgift.

21. Se till att användaren kan läsa men inte skriva eller köra textfilen du precis skrev.

22. Gör så att gruppmedlemmar till din textfilen bara kan läsa och skriva till filen, men inte köra den.

23. Lista alla filer i mappen "Computer/OS" och se till att storleken på dem visas på ett för en människa lättförståeligt sätt.

24. Gå till mappen som fil du nu läser finns i.

25. Ta bort mappanra "Animals" och "Computer".

26. Gå till din hemmapp.

Glöm inte att skicka in allt som är kvar som en zip fil till {{mail}}.
