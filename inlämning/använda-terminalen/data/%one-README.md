# {{name}}
 * **Grupp:** {{group-name}}
 * **Inlämningsdatum:** {{deadline}}
 * **Namn:** {{id}}
 * **Kurs:** {{course}}

## Regler


  * Du besvara frågorna som du finner i filerna i denna mapp enlig instruktionerna nedan.
  * Ändra inte på något om det inte är en del av en uppgift.  
  * När du är klar ska ta bort mappen "Animals" och "Computer".
  * Du ska sedan packa ihop hela mappen i en zip och bifoga den i ett mail.
  * Du ska skicka mailet till "magnus.kronnas@hassleholm.se".
  * Ämnet på brevet ska vara "{{name}}".
  * På första lektionstillfället efter inlämningsdagen kan du få svara på frågor om hur uppgiften lämnades in.
  * Blir du inte godkänd på denna inlämming kommer du få göra en komplettering som du får via mail.


**OBS!**
Är ämnet fel, hela uppgiften inte packat i en zip fil eller brevet skicka in för sent, kommer uppgiften inte att bedömas. *Undantag kan göras om det finns en särskild anledning.*
 
### För alla programmeringskod gäller
*Dessa regler gäller bara på Del II och bara om de är tillämbara*

  * Alla variabler har namn som visar vad de används till i programmet.
  * Du ska alltid kommentera din kod så den är lätt att förstå.
  * Du ska skriva minst en kommentar per uppgift och per funktion.
  * Koden på uppgifterna ska vara kort och lätt överskådlig.
  * Du ska inte sprida din kod innan den har blivit bedömd.

### För samarbete

Samarbete är bra och uppmuntras, men för att det inte ska påverka den individuella bedömningen gäller.

  * Ingen kod får kopieras från någon annan elev.
  * Bara kortare kodsnuttar får kopieras direkt från internet, internet sökningar är dock tillåtna och uppmuntras.
  * Diskussioner är bra, men undvik direkta frågor om hur en uppgift ska konkret lösas.
  * Rapporter direkta överträdelser till dessa regler till läraren, om du gör det muntligt till mig kan du vara anonym.

Brott mot dessa regler är fusk och kan enligt skolan policy för fusk leda till avstängning. Observerat att dessa uppgifter kryllar med olika metoder för att upptäcka om du har fuskat. Det finns dock enkla sätt att gå runt detta om du vill, men tänk på vad det innebär för dig själv. Om du bryr dig så lite om kunskapen som utbildningen försöker ge dig att du måste fuska, så ska du hitta på något annat att göra i livet.

## Bedömning

Din bedömning kommer bli den lägsta nivån som du får på de olika delarna nedan. Om du klarat en del på en högre nivå än de andra delen kommer ditt betyg bli det lägsta mellan betyget som är högre än ditt lägsta resultat. Med andra ord om du får E och A, så blir ditt resultat D.

**Observera att en bedömning på denna uppgift som visar att ditt resultat är F är också en F-varning. Du kommer då behöva göra komplettering för E nivå.**

## Bedömning

Din bedömning kommer bli den lägsta nivån som du klara nedan. Eller om du klarat en del på en högre nivå än den andra delen. Kommer ditt betyg bli det lägsta mellanbetyg som är högre än ditt lägsta resultat. Med andra ord om du får E och A, så blir ditt resultat D.

Observera att en bedömning som visar att ditt resultat är F är också en F-varning. du kommer då behöva göra komplettering för  E nivå.

## __Del I:__ Gör alla uppgifter om Linux kommandon

Du hittar uppgifterna som du ska göra i filen "Del-I-uppgifter.txt". Observera att du under varje uppgift ska skriva vilket kommando som du använder. Skriv kommandot i terminalen och kopiera in det under varje uppgift.

### Bedömning

#### E nivå:
  * Gör minst hälften av uppgifterna.

#### C nivå:
  * Du ska ha löst i stort sett alla uppgifter korrekta, men undantag för någon.
 
#### A nivå:
  * Du ska använda som mest ett kommando för att lösa en uppgift.
  * Alla instruktioner i detta dokument har följts.

### __Del II:__  Skapa ett eget script

Du hittar denna del i filen "Del-II-script.txt". Lös uppgiften genom att skriva ett enkelt bash script. Du får inte använda något annat språk en bash. Skriptet borde inte bli mer än ett par rader.

### Bedömning

Denna del har ingen E nivå har du klarat andra delar på E nivå kommer det bedömas som att du även klarat denna på E nivå.

#### C nivå:
  * Du ska påbörjat en lösning med några kommandon.
  * Koden följer reglerna ovan för kommentarer.
 
#### A nivå:
  * Ditt program löser uppgiften helt.
  * Det är välskrivet och i enlighet med reglerna.
   
---
__Copyright © 2018-{{year}}, Magnus Kronnäs__

__All rights reserved.__
