# {{examination-name}}
**Grupp:** {{group-name}}
**Inlämningsdatum:** {{deadline}}

## Regler

  * Du besvara frågorna i filern enlig instrutionerna nedan. 
  * Ändra inte på något om det inte är en del av en uppgift. 
  * När du är klar ska du packa ihop alla filer med zip och bifoga denna fil i ett mail.
  * Du ska skicka mailet till "{{mail}}".
  * Ämnet på brevet ska vara "{{examination-id}}".
  * På första lektionstillfället efter inlämningsdagen kan du få svara på frågor om uppgiften.
  * Blir du inte godkänd på denna inlämming kommer du få göra en komplettering enligt beskrivningen nedan.

**OBS!**
Är ämnet fel, hela uppgiften inte packat i en zip eller brevet skicka in för sent, kommer uppgiften inte att bedömas. *Undantag kan göras om det finns en skärskilt anledning.*
  
### För alla programmeringskod gäller

  * Alla variabler har namn som visar vad de används till i programmet.
  * Du ska skriva minst en kommentar per uppgift och per funktion.
  * Du ska alltid kommentera din kod så den är lätt att förstå.
  * Koden på uppgifterna är kort och lätt överskådlig.
  * Du ska undvika att sprida din kod.

## Bedömning

Det finns bara en del på denna uppgift så det kommer också bli ditt betyg. 

Observera att en bedömning som visar att ditt resultat är F är också en F-varning. Du kommer då behöva göra komplettering för E nivå.

## __Del I:__ Lös några små klurig kod

Gör tio uppgifter baserat på de uppgifter som du hittar i brevid denna README. De finns i filer som börjar på ett tal och filendelsen html. Du kan få max fyra poäng och det poängbedöms enligt följanbde:

  * **1 poäng** Om du är nära en lösning med du fär fel för visa testfall av ditt program eller din kod inte uppfyller inte ett krav som finns i uppgiften.
  * **1 poäng** Om du får rätt resultat och din kod uppfyller alla krav i uppgiften.
  * **1 poäng** Din kod är välskriven och du har löst uppgiften på bästa möjliga sätt.
  * **1 poäng** Din kod är välkommenterad och följer alla andra regler för koden enligt ovan.

Observera att det kan finns flera kriterier du måste uppnå för en viss nivå enligt nedan.
  
### E nivå:
  * Du ska ha fått minst 20 poäng.


### C nivå:
  * Du ska ha fått minst 28 poäng.
  * All din kod som du lämnar in måste vara väl kommenterad enligt reglerna.

### A nivå:
  * Du ska ha fått minst 36 poäng.
  * All kod måste vara effektiv och väl programmerad.

## Kompletering

Se uppgift instruktionerna i kursplaneringen för kompleteringar.
