# Inlämnings uppgift i Programmering 1

Se beskrviningen av denan uppgift som är länkad via SchoolSoft.

## Regler

  * Du besvara frågorna i filern enlig instrutionerna nedan. 
  * Skriv namn i varje fil.
  * Ändra inte på något om det inte är en del av en uppgift. 
  * När du är klar ska du packa ihop alla filer med zip.
  * Se till att filen har ditt alias som namn.
  * Du hittar en inlämningsläk på SchoolSoft.
  * På första lektionstillfället efter inlämningsdagen kan du få 
  svara på frågor om uppgiften.
  
### För alla programmeringskod gäller

  * Alla variabler har namn som visar vad de används till i programmet.
  * Du ska alltid kommentera din kod så den är lätt att förstå, se 
  till att åtminstone ha skrivit två till fyra kommentarer per uppgift.
  * Koden på uppgifterna är kort och lätt överskådlig.
  * Du ska inte visa din kod för någon annan.

## Bedömning

Se beskrvning av uppgiften som finns länkat i SchoolSoft.
