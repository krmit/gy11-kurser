# {{name}}
 * **Grupp:** {{group-name}}
 * **Inlämningsdatum:** {{deadline}}
 * **Namn:** {{id}}
 * **Kurs:** {{course}}

Denna uppgift ska träna dig i olika aspekter av javascript. De flesta uppgifterna använder sig av DOM och BOM, men det är inte nödvändigt att förstå dessa två ramverk för att kuna lösa upopgifterna. Följa bara det som står i tipsen.

## Regler

  * Du besvara frågorna i filern enlig instrutionerna nedan. 
  * Ändra inte på något om det inte är en del av en uppgiften. 
  * När du är klar ska du packa ihop alla filer med zip och bifoga denna fil i ett mail.
  * Du ska skicka mailet till "{{mail}}".
  * Ämnet på brevet ska vara "{{name}}".
  * På första lektionstillfället efter inlämningsdagen kan du få svara på frågor om uppgiften.
  * Blir du inte godkänd på denna inlämming kommer du få göra en komplettering enligt beskrivningen nedan.

**OBS!**
Är ämnet fel, hela uppgiften inte sparat i en zip eller brevet skicka in för sent, kommer uppgiften inte att bedömas. Du kommer då få en instrution för att komplettera.
  
### För alla programmeringskod gäller

  * Alla variabler har namn som visar vad de används till i programmet.
  * Du ska skriva minst en kommentar per uppgift och per funktion.
  * Du ska alltid kommentera din kod så den är lätt att förstå.
  * Koden på uppgifterna är kort och lätt överskådlig.
  * Du ska undvika att sprida din kod.

## Bedömning

Din bedömning kommer bli den lägsta nivån som du får på de olika delarna nedan. Om du klarat en del på en högre nivå än de andra delen kommer ditt betyg bli det lägsta mellanbetyget som är högre än ditt lägsta resultat. Med andra ord om du får E och A, så blir ditt resultat D. 

Observera att en bedömning på denna uppgift som visar att ditt resultat är F är också en F-varning. Du kommer då behöva göra komplettering för E nivå.

## __Del I:__ Lös några små kod uppgifter

Gör tio uppgifter som du hittar brevid denna README.md fil. De finns i de filer som börjar på ett tal och har filändelsen html.

### Bedömning

Du kan få max fyra poäng och det poängbedöms enligt följanbde:

  * **1 poäng** Om du är nära en lösning med du får fel för visa testfall av ditt program eller din kod inte uppfyller inte ett krav som finns i uppgiften.
  * **1 poäng** Om du får rätt resultat och din kod uppfyller alla krav i uppgiften.
  * **1 poäng** Din kod är välskriven och du har löst uppgiften på bästa möjliga sätt.
  * **1 poäng** Din kod är välkommenterad och följer alla andra regler för koden enligt ovan.

Observera att det kan finns flera kriterier du måste uppnå för en viss nivå enligt nedan.
  
### E nivå:
  * Du ska ha fått minst 24 poäng.

### C nivå:
  * Du ska ha fått minst 30 poäng.

### A nivå:
  * Du ska ha fått minst 36 poäng.

## __Del II:__ Skriv ett eget program

Gör ett program fungerar som ett exempel på så många olika språkligafunktioner hos javascript som möjligt. Programmet måste inte vara användbart till någont, men ska fungera som ett exempel på de saker som nämns nedan.

Du kan antingen skriva ditt program med DOM och BOM eller som ett node program på din labbdator. Men det är viktigt att du kommenterar din kod, skärskilt de delar som du ska ge exempel på enligt bedömningern nedan.

### Instruktioner
   * Programmet får inta användas i GA eller i någon anna kurs.
   * När du är klar sparar du ditt program och **alla** filerna som behövs för att köra programmer i en mapp "mittExempel".
   * Denna mapp ska ligga i samma mapp som uppgifterna som du har löst och lämnas in samtidigt som upppgifterna i Del I.

### Bedömning

Denna del har ingen E nivå har du klarat andra delar på E nivå kommer det bedömas som att du även klarat denna på E nivå. Om du får en fråga ska du själv kunna motivera varför du uppfyllt kraven nedan.

### C nivå:
  * Du ska ge exempel på *setPrototypeOf*, *this*, *callback* och *new*.
  * Koden fölger reglerna ovan.
  * Ditt program fungerar utan errors.
  * Ditt program går att använda.
  * Du har kommenterat vad din kod gör för något.
  
### A nivå:
  * Du ska ge exempel på *await* och *map*.
  * Du har följt alla instruktioner ovan.
  * Koden är effektiv och väldesignad.
  * Koden är lättläst och ser bra ut.
  * Ditt program implementerar allt som du beskriver i dina kod kommentarer.
