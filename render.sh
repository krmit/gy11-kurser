#/bin/sh

# Remove old html files, but not in "inlämning"
find Common Datorteknik Elektronik Programmering-1 Programmering-2 Webbserverprogrammering-1 Webbutveckling-1 -type f | grep "\.html" | xargs rm

# Go through all directories and creat html files.
find . -type d -not -name "image" -not -name "Mall" -not -name "mall" -not \( -name ".git*" -prune \) -exec bash -c "echo -n ".";cd {};ghmd *.md;" \;
echo "!";
