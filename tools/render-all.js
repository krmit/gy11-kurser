#!/usr/bin/node
"use strict";
const fs = require("fs-extra");
require("colors");
const glob = require("glob");
const { execSync } = require('child_process');

const { readdirSync, statSync } = require('fs');
const { join, resolve } = require('path');

module.exports.renderAll = renderAll; 

function renderAll() {
const page_paths = glob.sync(__dirname+"/../!(tools|Mall)/**/*.md");
console.log(page_paths);
console.log("\nPages:\n".green.bold);

for(let page of page_paths) {
    execSync(`ghmd ${page}`);
    console.log(page.green);
}
}

if (require.main === module) {
    renderAll();
}



